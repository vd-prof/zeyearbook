import fitz
import sys
import json
import base64
import tkinter as tk
from tkinter import ttk
from io import BytesIO
from PIL import Image, ImageTk



    
def insert_bloc(arbre, struct, parent):
    if isinstance(struct, list):
        for k in range(len(struct)):
            substruct = struct[k]
            if (not isinstance(substruct, list)) and (not isinstance(substruct, dict)):
                arbre.insert(parent, "end", text="#"+str(k), values=(str(substruct),))
            else:
                msg = "ARRAY" if isinstance(substruct, list) else "DICT"
                msg += "(" + str(len(substruct)) + ")"
                child = arbre.insert(parent, "end", text="#"+str(k), values=(msg,), open=False)
                insert_bloc(arbre, substruct, child)
    elif isinstance(struct, dict):
        for k in struct:
            substruct = struct[k]
            if (not isinstance(substruct, list)) and (not isinstance(substruct, dict)):
                arbre.insert(parent, "end", text=str(k), values=(str(substruct),))
            else:
                msg = "ARRAY" if isinstance(substruct, list) else "DICT"
                msg += "(" + str(len(substruct)) + ")"
                child = arbre.insert(parent, "end", text=str(k), values=(msg,), open=False)
                insert_bloc(arbre, substruct, child)




    
class Win:
    def __init__(self):
        self.Win = tk.Tk()
        self.Win.title("PDF Structure")
        
        parent = ttk.Frame(self.Win)
        parent.pack(side=tk.TOP, fill=tk.BOTH, expand=True)
        
        self.label = ttk.Label(self.Win, text="chemin: ", anchor=tk.W)
        self.label.pack(side=tk.BOTTOM, fill=tk.X, expand=False)
        
        grandparent = ttk.Frame(parent)
        grandparent.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        grandparent.grid_columnconfigure(0, weight=1)
        grandparent.grid_rowconfigure(0, weight=1)
        
        parent_droite = ttk.Frame(parent)
        parent_droite.pack(side=tk.RIGHT, fill=tk.Y, expand=False)
        
        self.canvas_bbox = tk.Canvas(parent_droite, width=300, height=400)
        self.canvas_bbox.pack(side=tk.TOP, fill=tk.NONE, expand=False)
        
        self.canvas_photo = tk.Canvas(parent_droite, width=300, height=400)
        self.canvas_photo.pack(side=tk.BOTTOM, fill=tk.NONE, expand=False)
        self.photo = None
        
        self.tree = ttk.Treeview(grandparent, columns = ("#0", "#1"), show="tree headings", selectmode="browse")
        self.tree.column("#0", width = 200, stretch=True)
        self.tree.column("#1", width = 400, stretch=True)
        self.tree.heading('#0', text='structure')
        self.tree.heading('#1', text='valeur')
        self.tree.bind('<ButtonRelease-1>', lambda x:self.selectItem(x))
        self.tree.grid(row=0, column=0, sticky='wens')

        self.pages = []

    def populate_page(self, dico):
        self.pages.append(dico)
        maxblocks = len(dico["blocks"])
        pagetree = self.tree.insert("", "end", text="Page " + str(p), values=(str(maxblocks)+" elements",), open=False)
        insert_bloc(self.tree, dico, pagetree)

    def bbox_rect(self, p, boxes=[]):
        self.canvas_bbox.delete("all")
        width = self.canvas_bbox.winfo_width()
        height = self.canvas_bbox.winfo_height()
        pwidth = self.pages[p]["width"]
        pheight = self.pages[p]["height"]
        dxy = min(width/pwidth, height/pheight)
        self.canvas_bbox.create_rectangle(0,0 , pwidth*dxy,pheight*dxy, fill="white", tag="page")
        for k in range(len(self.pages[p]["blocks"])):
            cbox = list(map(float, self.pages[p]["blocks"][k]["bbox"]))
            x0, y0, x1, y1 = map(lambda x:x*dxy, cbox)
            if not cbox in boxes:
                self.canvas_bbox.create_rectangle(x0,y0 , x1,y1 , fill="green", tag="boite")
        for box in boxes:
            x0, y0, x1, y1 = map(lambda x:x*dxy, box)
            self.canvas_bbox.create_rectangle(x0,y0 , x1,y1 , fill="red", tag="boite")



    def tree_walk(self, item, box_list):
        """
        Parcours récursivement les enfants de l'item dans l'arbre à la recherche de bbox à mettre en valeur.
        Les box trouvées sont append à box_list
        Si on trouve une image, on retourne ses data pour affichage sinon, on retourne None
        """
        val = None
        jitem = self.tree.item(item)
        if jitem["text"].startswith("Page ") or jitem["text"] == "blocks":
            pass
        elif jitem["text"] == "image":
            val = base64.b64decode(jitem["values"][0])
        elif jitem["text"] == "bbox":
            childs = self.tree.get_children(item)
            x0 = float(self.tree.item(childs[0])["values"][0])
            y0 = float(self.tree.item(childs[1])["values"][0])
            x1 = float(self.tree.item(childs[2])["values"][0])
            y1 = float(self.tree.item(childs[3])["values"][0])
            box_list.append([x0, y0, x1, y1])
        else:
            childs = self.tree.get_children(item)
            for child in childs:
                newval = self.tree_walk(child, box_list)
                if newval != None:
                    val = newval
        return val
            

    def update_label(self, item):
        """
        affiche le chemin pour arriver à cet item dans l'arbre.

        Retourne la page courante du document.
        """
        reformat = lambda txt: "[" + (txt[1:] if txt.startswith("#") else ('"' + txt + '"')) + "]"
        msg = ""
        jitem = self.tree.item(item)
        while not jitem["text"].startswith("Page"):
            msg = reformat(jitem["text"]) + msg
            item = self.tree.parent(item)
            jitem = self.tree.item(item)
        msg = "Chemin: page[" + jitem["text"][5:] + "]" + msg
        self.label["text"] = msg
        return int(jitem["text"][5:])
    
    
    def selectItem(self, event):
        item = self.tree.focus()
        jitem = self.tree.item(item)
        boxes = []
        data = self.tree_walk(item, boxes)
        if data != None:
            image = Image.open(BytesIO(data))
            self.photo = ImageTk.PhotoImage(image)
            width = self.canvas_photo.winfo_width()
            height = self.canvas_photo.winfo_height()
            self.canvas_photo.create_image(width/2, height/2, image=self.photo, anchor=tk.CENTER)
        else:
            self.photo = None
        p = self.update_label(item)
        self.bbox_rect(p, boxes)



if len(sys.argv) != 2:
    print(sys.argv[0] + " <PDF-filename>")
    sys.exit(1)


fenetre = Win()

doc = fitz.Document(sys.argv[1])
for p in range(doc.pageCount):
    page = doc.load_page(p)
    dico = json.loads(page.get_text('json'))
    fenetre.populate_page(dico)




fenetre.Win.mainloop()
