***Ze Year Book*** est un projet du lycée pour rassembler tous les trombinoscopes des élèves ainsi que les
animations de l'année scolaire dans un (gros) livre pour que les élèves et les profs puissent garder une trace.
Une sorte de capsule temporelle en somme.

Ces scripts permettent l'extraction des photos et des noms du pdf d'origine et création d'une nouvelle page au format voulu.
On passe d'un pdf à un autre pdf.
L'idée est de ne pas déteriorer la qualité des images, de rassembler les photos d'une classe sur une seule page et
d'agencer tout ça comme on veut.


L'objectif est de faire gagner du temps aux collègues qui devaient faire ça à la main au début. J'espère que ce but est atteint !


Prérequis:
==========

Pour faire fonctionner ce script, il vous faut **Python3** avec les bibliothèques:
   - **PyMuPDF**, pour l'extraction de donnée
   - **PIL**, pour la détection des chaises vides
   - **json**, pour le stockage des informations

Sous linux Mint, les paquets se nomment **pyhton3-fitz**, **python3-pil**, **python3-tk**,
 **python3-pil.imagetk** et **python3-simplejson**



Utilisation:
============

Tout se passe à la souris. Il suffit de lancer le script

    $ python3 trombi.py


Il est peut-être possible qu'un double clic sur le ficher `trombi.py` fonctionne aussi
sur certains systèmes d'exploitation.


Quelques explication:
--------------------

Dans les paramètres,
  - le **fichier source** est le fichier d'origine, produit par les outils d'Index Education.
  - le **dossier de travail** est le dossier dans lequel sont stockés les fichiers intermédiaires.
    Il s'aigit des photos (dans un sous-dossier _photo_) et d'un fichier texte (d'extension trb)
    contenant tous les paramètres d'une classe.
  - le **dossier destination** est le dossier dans lequel sont stockés les pdf produits.


Au niveau des actions, il y en a 4:
  - l'**extraction** prend le fichier d'origine et extrait les informations.
    D'abord les textes et les images, puis on filtre les textes et les images inutiles,
    ensuite on procède à l'association nom-image et eleve-classe;
    enfin on sauve tout ça dans le dossier de travail.
  - les **paramètres généraux** permettent de spécifier des propriétés sur toutes les classes
    dans le dossier de travail: les marges, la disposition, les fonts, l'image de chaise vide,...
    Toutes les classes du dossier de travail seront affectées !
  - les **paramètres individuels** permettent de spécifier ces mêmes paramètres mais uniquement
    sur la classe en cours. On peut aussi agir sur les élèves:
      - enlever, déplacer, ajouter un élève ;
      - déclarer son image comme chaise vide ou non
      - corriger son nom ou le mettre sur 2 ou 3 lignes.
  - la **génération** permet enfin de générer le fichier pdf en accord avec les paramètres
    fixés avant.


Quelques remarques:
------------------

  1. Le fichier trb est un simple fichier json.
     On a volontairement rajouter des espaces et sauts de ligne pour qu'il soit plus facilement
     lisible et modifiable par un humain.
     Voir `trombi_trb` (classe `TrbClasse`) pour plus d'explixations.

  2. Si une image est déclarée "chaise vide" elle n'est pas affichée. Si le paramètre de la classe
     l'indique, soit on ne met juste rien, soit on affiche une image standard de chaise vide.
     Cette image est configurable.

  3. Ces scripts ne sont certainement pas sans défaut, notamment au niveau des explications,
     de l'ergonomie ou des bugs.
     Toutes les critiques constructives et les demandes particulières sont les bienvenues.
     Il ne faut surtout pas hésiter. Si c'est trop fantaisiste ou trop compliqué, je vous le dirai.
