import tkinter as tk
from tkinter import ttk

from trombi_page import PanneauPage
from trombi_trb import TrbClasse
from trombi_eleve import PanneauEleve




class ModificationPanel(ttk.Frame):
    """
    Panneau général contenant toute l'interface.

    Contient
      - un panneau supérieur avec quelques infos
      - un panneau latéral contenant des options générales et l'image de la page
      - un panneau latéral contenant des options particulières d'un élève
    """

    def __init__(self, parent):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
        """
        super().__init__(parent)

        self.pan_sup = ModifySelectionPanel(self)
        self.pan_page = PanneauPage(self, real_data=True)

        self.pan_sup.pack(side=tk.TOP, expand=True, fill=tk.X)
        self.pan_page.pack(side=tk.LEFT, expand=False, fill=tk.BOTH)


        self.pan_page.set_info_panel(self)

        self.trb_list = []
        self.current_trb_file = None



    def set_trb_list(self, liste):
        """
        Crée la liste des TrbClasse accéssible à partir des noms de fichier
        Met à jour l'interface en conséquence.

        params:
          - liste: [list] liste de noms de fichiers (format TRB)

        return: None
        """
        for fic in liste:
            try:
                self.trb_list.append( TrbClasse(ficname=fic) )
            except Exception as e:
                print("WARNING: unsupported format: " + fic)
                print(e)
        self.trb_list.sort(key=lambda x:x.nom_classe)
        noms = list(map(lambda x:x.nom_classe, self.trb_list))
        self.pan_sup.populate_trb(noms)



    def reload_trb(self):
        """
        recharge les données du trb actuel.
        Surtout utile en cas de changement de nom.
        """
        self.pan_sup.load_trb(self.current_trb_file)



    def select_trb(self, index):
        """
        Bascule vers un autre trb de la liste

        params:
          - index: [int] indice du nouveau trb dans la liste

        return: None
        """
        trb = self.trb_list[index]
        self.current_trb_file = trb

        self.pan_sup.load_trb(trb)
        self.pan_page.load_trb(trb)



    def show_boxes_version(self):
        """
        Quelle version doit être afficher ? Version finale ou avec les boites ?

        return True si avec les boites, False si version finale
        """
        return self.pan_sup.show_boxes_version()



    def redraw_page(self):
        self.pan_page.redrawn_needed()



    def save_trb(self, only_current=True):
        """
        Sauve le trb dans un fichier.

        return: None
        """
        self.pan_page.save_trb()
        self.current_trb_file.sauve()
        print("saved trb in " + self.current_trb_file.fichier)
        if not only_current:
            for trb in self.trb_list:
                if trb != self.current_trb_file:
                    trb.sauve()
                    print("saved trb in " + trb.fichier)









class ModifySelectionPanel(ttk.Frame):
    """
    Panneau supérieur de l'interface.

    Contient:
      - une liste déroulante avec l'ensemble des fichiers trb accéssibles dans le dossier
      - une étiquette avec la classe et le nombre d'élèves
      - un bouton pour enregistrer les changements
    """

    def __init__(self, parent):
        """
        Initialise l'objet

        params:
          - parent: [ModificationPanel] parent du GUI
        """
        super().__init__(parent)
        self.parent = parent

        self.savbut = ttk.Button(self, text="Enregistrer", command=self.save_trb)

        self.savallbut = ttk.Button(self, text="Enregistrer tous", command=self.save_all_trb)

        self.selbox = ttk.Combobox(self)
        self.selbox.state(['readonly'])
        self.selbox.bind('<<ComboboxSelected>>', self.change_trb)

        self.affbox = ttk.Combobox(self, width=25)
        self.affbox.state(['readonly'])
        self.affbox['values'] = ("Afficher les boites réservées", "Aficher la version finale")
        self.affbox.current(0)
        self.affbox.bind('<<ComboboxSelected>>', self.change_affichage)

        self.label = ttk.Label(self, text="0 élève", anchor=tk.W)
        lbl = ttk.Label(self, text="classe:")

        self.savallbut.pack(side=tk.RIGHT, padx=15)
        self.savbut.pack(side=tk.RIGHT, padx=15)
        self.affbox.pack(side=tk.RIGHT, padx=150)
        lbl.pack(side=tk.LEFT, padx=15)
        self.selbox.pack(side=tk.LEFT, fill=tk.X, padx=(0,15))
        self.label.pack(side=tk.LEFT)



    def populate_trb(self, trb_list):
        """
        mise à jour de la liste déroulante

        params:
         - trb_list: [list] liste de TrbClasse

        return: None
        """
        self.selbox['values'] = trb_list
        if len(trb_list) == 1:
            self.selbox.state(['disabled'])
        if len(trb_list) > 0:
            self.selbox.current(0)
            self.parent.select_trb(0)



    def show_boxes_version(self):
        """
        Quelle version doit être afficher ? Version finale ou avec les boites ?

        return True si avec les boites, False si version finale
        """
        return self.affbox.current() == 0



    def change_trb(self, event):
        """
        Gestion du changement de trb via la combobox

        params:
          - event: l'event envoyé par la combobox

        return: None
        """
        self.parent.select_trb(self.selbox.current())



    def change_affichage(self, event):
        """
        Gestion du changement d'affichage via la combobox

        params:
          - event: l'event envoyé par la combobox

        return: None
        """
        self.parent.redraw_page()



    def save_trb(self):
        """
        Sauve le fichier

        return: None
        """
        self.parent.save_trb(only_current=True)



    def save_all_trb(self):
        """
        Sauve tous les fichiers trb

        return: None
        """
        self.parent.save_trb(only_current=False)



    def load_trb(self, trb_data):
        """
        Mise à jour de l'étiquette avec les informations du trb

        params:
          - trb_data: [TrbClasse] les données trb

        return: None
        """
        if trb_data != None:
            tot = len(trb_data.eleves)
            self.label['text'] = str(tot) + " élève" + ("s" if tot > 0 else "")
            if self.selbox.get() != trb_data.nom_classe:
                index = self.selbox.current()
                values = list(self.selbox['values'])
                values[index] = trb_data.nom_classe
                self.selbox['values'] = values
                self.selbox.current(index)














if __name__ == "__main__":
    import sys
    import os

    wind = tk.Tk()
    wind.title("Modification des trombis")

    panneau = ModificationPanel(wind)
    panneau.pack(fill="both", expand=True)

    fichiers = []
    if len(sys.argv) == 1:
        # rien
        fichiers = [os.path.abspath(fic) for fic in os.listdir(os.getcwd())]
    elif len(sys.argv) == 2 and os.path.isdir(sys.argv[1]):
        # un seul repertoire
        fichiers = [os.path.abspath(os.path.join(sys.argv[1], fic)) for fic in os.listdir(sys.argv[1])]
    else:
        # un ou des fichiers
        fichiers = [os.path.abspath(fic) for fic in sys.argv[1:]]

    listing = []
    for fic in fichiers:
        if fic.endswith(".trb"):
            if os.path.isfile(fic):
                listing.append(fic)

    panneau.set_trb_list(listing)

    wind.mainloop()

#
#
# END
#
#
