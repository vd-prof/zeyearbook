import json
import os

from PIL import Image




def mezydonc(obj, dico, key):
    """
    Fonction pour mettre à jour un objet à partir d'un dictionnaire.
    Si dico[key] existe, on met à jour obj.key
    Sinon, on touche à rien.

    Petite subtilité: les clés du dico sont "de-cette-forme"
    alors que les attributs de l'objet sont obj.de_cette_forme_la

    Il faut donc convertir les '-' en '_'

    params:
      - obj: objet à mettre à jour
      - dico: [dict] dictonnaire des valeurs
      - key: [string] clé du dictionnaire

    return: None
    """
    if key in dico:
        setattr(obj, key.replace('-', '_'), dico[key])









class TrbEleve:
    """
    Données trb d'un élève.

      - nom: [list] tableau de 2 ou 3 string
      - photo: [string] chemin absolu de l'image de l'élève. chaine vide si pas de fichier.
      - chaise_vide: [booléen] True si c'est une chaise vide, False si c'est une vraie image
    """

    def __init__(self, classe, data=None, basedir=None):
        """
        Initialise l'objet.
        Le chemin de l'image est stocké de manière relative dans le fichier mais tous les objets qui utilisent
        un TrbEleve partent du principe que les chemins sont absolus.
        `basedir` sert à donner le dossier de base des images pour pouvoir reconstituer la chemin absolu.

        params:
          - classe: [TrbClasse] la classe qui contient l'élève.
          - data: [dict | string] data json des données
          - basedir: [string] repertoire racine
        """
        self.nom = []
        self.photo = ""
        self.chaise_vide = True

        self.image_ratio = 0 # ratio hauteur / largeur de l'image de l'élève.

        self.classe = classe

        if data != None:
            self.load(data)



    def get_image_area(self, max_width, max_height):
        """
        Donne la surface occupée par l'image en gardant les proportions avec des contraintes de taille maximale.
        Les tailles peuvent être négatives : la surface sera nulle.

        params:
          - max_width: [number] largeur maxi autorisée
          - max_height: [number] hauteur maxi autorisée

        return: la surface maxi de l'image
        """
        if max_width <= 0 or max_height <= 0 or self.image_ratio == 0:
            return 0
        if self.image_ratio * max_width <= max_height:
            return self.image_ratio * max_width * max_width
        return max_height * max_height / self.image_ratio



    def load(self, data):
        """
        Charge des données.
        Vérifie si les données sont valides.
        Charge l'image et met à jour son ratio.

        params:
          - data: [dict | string] Les données. dict ou une chaine de caractères compatible json
          - basedir: [string] répertoire de travail pour les chemins relatifs des images

        return: None
        """
        dico = json.loads(data) if type(data) == str else data
        mezydonc(self, dico, 'nom')
        mezydonc(self, dico, 'photo')
        mezydonc(self, dico, 'chaise-vide')
        basedir = os.path.dirname(self.classe.fichier)
        if self.photo != "":
            try:
                self.photo = os.path.join(basedir, self.photo)
                image = Image.open(self.photo)
                width, height = image.size
                self.image_ratio = height / width
            except:
                print("Image non trouvée: " + self.photo)
                self.image_ratio = 0



    def to_str(self, prefix=""):
        """
        converti l'objet en chaine de caractères type json

        params:
          - prefix: [string] préfix de toutes les lignes
          - basedir: [string] répertoire de travail pour les chemins relatifs des images.

        return: chaine de caractères de type json représentant les données trb
        """
        basedir = os.path.dirname(self.classe.fichier)
        msg  = prefix + '{\n'
        msg += prefix + '\t"nom": [' + ', '.join('"' + n + '"' for n in self.nom) + '],\n'
        msg += prefix + '\t"photo": "' + (os.path.relpath(self.photo, basedir) if self.photo != "" else "") + '",\n'
        msg += prefix + '\t"chaise-vide": ' + ('true' if self.chaise_vide else 'false') + '\n'
        msg += prefix + '}'
        return msg









class TrbClasse:
    """
    Données trb d'une classe

      - unite: [string] unité dans laquelle sont données les distances
      - nom_classe: [string] nom de la classe
      - affiche_nom_classe: [booléen] pour savoir si on affiche ou pas le nom de la classe
      - hauteur_nom_classe: [flaot] place réservée (en unite) pour le nom de la classe si besoin
      - taille_font_classe: [int] taille de la police de caractère du nom de la classe
      - nom_font_classe: [string] nom de la police de caractères utilisée pour le nom de la classe
      - nb_lignes: [int] nombre de lignes d'élèves (0: calcul automatique du meilleur)
      - nb_colonnes: [int] nombre de colonnes d'élèves (0: calcul automatique du meilleur)
      - image_chaise_vide: [booléen] si True affiche une image de chaise vide sinon n'affiche rien
      - fichier_chaise_vide: [string] chemin absolu de l'image de chaise vide
      - hauteur_nom_eleve: [float] hauteur en unité réservée dans la boite élève pour son nom
      - taille_font_eleve: [int] taille de la police de caractère du nom de l'élève
      - nom_font_eleve: [string] nom de la police de caractères utilisée pour le nom de l'élève
      - marge_horizontale_eleves: [float] largeur en unité entre boite d'élève
      - marge_verticale_eleves: [float] hauteur en unité entre boite d'élève
      - marge_horizontale_page: [float] largeur en unité entre le bord de la page et les boites élève
      - marge_verticale_page: [float] hauteur en unité entre le bord de la page et les boites élève
      - largeur_page: [float] largeur en unité de la page
      - hauteur_page: [float] hauteur en unité de la page
      - eleves: [list] liste d'élèves sous forme de trb-eleve
    """

    def __init__(self, ficname=None, json_data=None, trb_data=None, basedir=None):
        """
        Initialise l'objet
        """
        if sum((1 if var != None else 0) for var in (ficname, json_data, trb_data)) > 1:
            raise Exception("Objet TrbClasse mal défini: (ficname, json_data, trb_data) sont mutuellement exclusifs")

        # Objet pas défaut
        self.fichier = None

        self.unite = "mm"

        self.nom_classe = ""
        self.affiche_nom_classe = True
        self.hauteur_nom_classe = 15
        self.taille_font_classe = 20
        self.nom_font_classe = "helv"

        self.nb_lignes = 0
        self.nb_colonnes = 0

        self.image_chaise_vide = True
        self.fichier_chaise_vide = ""

        self.hauteur_nom_eleve = 10
        self.taille_font_eleve = 7
        self.nom_font_eleve = "helv"

        self.marge_horizontale_eleves = 10
        self.marge_verticale_eleves = 10

        self.marge_horizontale_page = 20
        self.marge_verticale_page = 20

        self.largeur_page = 210
        self.hauteur_page = 297

        self.eleves = []

        if ficname != None:
            self.load_file(ficname)
        elif json_data != None:
            self.load_json(json_data, basedir)
        elif trb_data != None:
            self.load_trb(trb_data, basedir)



    def load_file(self, ficname):
        """
        Charge les données trb à partir d'un fichier.

        params:
          - ficname: [string] chemin du fichier.

        return: None
        """
        content = None
        with open(ficname, 'r') as fic:
            content = fic.read()
        self.load_json(content, os.path.dirname(ficname))
        self.fichier = ficname



    def load_json(self, data, basedir=None):
        """
        Charge les données trb à partir d'un json.
        Les données sont sous forme d'une chaine de caractère représentant un json.

        params:
          - data: [string] représentant le json des données
          - basedir: [string] le chemin qui sert de base aux chemins relatifs des données. Permet de reconstituer des chemins absolus.

        return: None
        """
        dico = json.loads(data)
        var = ('unite',
               'nom-classe',
               'affiche-nom-classe',
               'hauteur-nom-classe',
               'taille-font-classe',
               'nom-font-classe',
               'nb-lignes',
               'nb-colonnes',
               'image-chaise-vide',
               'fichier-chaise-vide',
               'hauteur-nom-eleve',
               'taille-font-eleve',
               'nom-font-eleve',
               'marge-horizontale-eleves',
               'marge-verticale-eleves',
               'marge-horizontale-page',
               'marge-verticale-page',
               'largeur-page',
               'hauteur-page')
        for v in var:
            mezydonc(self, dico, v)
        self.fichier = "trombi-" + self.nom_classe + ".trb"
        if basedir != None:
            self.fichier = os.path.join(basedir, self.fichier)
        for elev in dico['eleves']:
            self.eleves.append(TrbEleve(classe=self, data=elev))



    def load_trb(self, data, basedir=None):
        """
        Charge des données à partir d'un trb existant.
        Vérifie si les données sont valides.
        Ne met pas à jour les élèves.

        params:
          - data: [TrbClasse]
          - basedir: [string] le chemin qui sert de base aux chemins relatifs des données. Permet de reconstituer des chemins absolus.

        return: None
        """
        self.unite  = data.unite
        self.affiche_nom_classe = data.affiche_nom_classe
        self.hauteur_nom_classe = data.hauteur_nom_classe
        self.taille_font_classe = data.taille_font_classe
        self.nom_font_classe = data.nom_font_classe
        self.nb_lignes = data.nb_lignes
        self.nb_colonnes = data.nb_colonnes
        self.image_chaise_vide = data.image_chaise_vide
        self.fichier_chaise_vide = data.fichier_chaise_vide
        self.hauteur_nom_eleve = data.hauteur_nom_eleve
        self.taille_font_eleve = data.taille_font_eleve
        self.nom_font_eleve = data.nom_font_eleve
        self.marge_horizontale_eleves = data.marge_horizontale_eleves
        self.marge_verticale_eleves = data.marge_verticale_eleves
        self.marge_horizontale_page = data.marge_horizontale_page
        self.marge_verticale_page = data.marge_verticale_page
        self.largeur_page = data.largeur_page
        self.hauteur_page = data.hauteur_page
        self.fichier = "trombi-" + self.nom_classe + ".trb"
        if basedir != None:
            self.fichier = os.path.join(basedir, self.fichier)



    def sauve(self):
        """
        Sauve physiquement les données dans le fichier.
        Si le nom de la classe a changé, on supprime l'ancien fichier et on écrit le nouveau.
        ATTENTION: on supprime sans vérifier, on écrase sans vérifier !

        return: None
        """
        nomfic = "trombi-" + self.nom_classe + ".trb"
        if self.fichier == None:
            self.fichier = nomfic
        elif os.path.basename(self.fichier) != nomfic:
            if os.path.exists(self.fichier):
                os.remove(self.fichier)
            self.fichier = os.path.join(os.path.dirname(self.fichier), nomfic)
        with open(self.fichier, 'w') as fic:
            content = self.to_str()
            fic.write(content)



    def n2s(self, nb):
        """
        Number To String.
        Transforme un nombre en string.
        int -> str: normal
        float -> str: vérifie d'abord si le float est un int. Dans ce cas float -> int -> str.

        Exemple:
          n2s(4) -> 4
          n2s(1.3) -> 1.3
          n2s(5.0) -> 5

        params:
          - nb: [number] Le nombre à convertir

        return: [string]
        """
        txt = str(nb)
        if "." in txt:
            i = txt.index(".")
            if txt[i+1:] == "0":
                txt = txt[:i]
        return txt



    def to_str(self, prefix=""):
        """
        Converti l'objet en chaine de caractère type json.

        params:
          - prefix: [string] le prefixe de chaque ligne
          - basedir: [string] le répertoire de travail pour transformer les chemins absolus en chemins relatifs.

        return: chaine de caractères de type json représentant les données trb
        """
        basedir = os.path.dirname(self.fichier)
        msg  = prefix + '{\n'
        msg += prefix + '\t"nom-classe": "' + self.nom_classe + '",\n'
        msg += prefix + '\t"affiche-nom-classe": "' + ("true" if self.affiche_nom_classe else "false") + '",\n'
        msg += prefix + '\t"nb-lignes": ' + self.n2s(self.nb_lignes) + ',\n'
        msg += prefix + '\t"nb-colonnes": ' + self.n2s(self.nb_colonnes) + ',\n'
        msg += prefix + '\t"image-chaise-vide": ' + ('true' if self.image_chaise_vide else 'false') + ',\n'
        msg += prefix + '\t"fichier-chaise-vide": "' + (os.path.relpath(self.fichier_chaise_vide, basedir) if self.fichier_chaise_vide != "" else "") + '",\n'
        msg += prefix + '\t"unite": "' + self.unite + '",\n'
        msg += prefix + '\t"hauteur-nom-classe": ' + self.n2s(self.hauteur_nom_classe) + ',\n'
        msg += prefix + '\t"hauteur-nom-eleve": ' + self.n2s(self.hauteur_nom_eleve) + ',\n'
        msg += prefix + '\t"taille-font-classe": ' + self.n2s(self.taille_font_classe) + ',\n'
        msg += prefix + '\t"taille-font-eleve": ' + self.n2s(self.taille_font_eleve) + ',\n'
        msg += prefix + '\t"nom-font-classe": "' + self.nom_font_classe + '",\n'
        msg += prefix + '\t"nom-font-eleve": "' + self.nom_font_eleve + '",\n'
        msg += prefix + '\t"marge-horizontale-eleves": ' + self.n2s(self.marge_horizontale_eleves) + ',\n'
        msg += prefix + '\t"marge-verticale-eleves": ' + self.n2s(self.marge_verticale_eleves) + ',\n'
        msg += prefix + '\t"marge-horizontale-page": ' + self.n2s(self.marge_horizontale_page) + ',\n'
        msg += prefix + '\t"marge-verticale-page": ' + self.n2s(self.marge_verticale_page) + ',\n'
        msg += prefix + '\t"largeur-page": ' + self.n2s(self.largeur_page) + ',\n'
        msg += prefix + '\t"hauteur-page": ' + self.n2s(self.hauteur_page) + ',\n'
        msg += prefix + '\t"eleves": [\n'
        for k,el in enumerate(self.eleves):
            if k != 0:
                msg += ',\n'
            msg += prefix + el.to_str(prefix=prefix+'\t')
        msg += ']\n'
        msg += prefix + '}'
        return msg


#
#
# END
#
#
