import fitz
import os
import time
import json
import base64
import unidecode
from math import sqrt
from io import BytesIO
from PIL import Image

import tkinter as tk
from tkinter import ttk

from trombi_trb import *
from trombi_thread import *
from trombi_page import PanneauPage




class ExtractionPanel(ttk.Frame):
    """
    Paneau général contenant tout ce qu'il faut pour l'extraction:
    le ExtractionManager et le TaskManagerPanel.
    """

    def __init__(self, parent):
        """
        params:
          - parent: parent du GUI
        """
        super().__init__(parent)
        self.logpan = TaskManagerPanel(self)
        self.extractor = ExtractionManager(self.logpan, parent)

        self.logpan.pack(fill=tk.BOTH, expand=True)



    def extract(self, pdf_file, dest):
        """
        Lance l'extraction

        params:
          - pdf_file: [string] le fichier à convertir
          - dest: [string] le dossier de destination du trb et des images
        """
        self.logpan.add_task([pdf_file, dest])









class PdfObject:
    """
    Objet générique contenu dans un pdf.
    On stocke la bbox et le numéro de page pour savoir d'où ça vient.
    """

    """ point de référence situé au North-West de la boite """
    NW = 0

    """ point de référence situé au North-East de la boite """
    NE = 1

    """ point de référence situé au South-East de la boite """
    SE = 2

    """ point de référence situé au South-West de la boite """
    SW = 3


    def __init__(self, bbox, pno):
        """
        Initialise l'objet

        params:
          - bbox: [list] liste des 4 coordonnées de la boite
          - pno: [int] le numéro de la page
        """
        self.box = bbox
        self.pno = pno



    def get_point(self, direction):
        """
        Donne le point indiqué de la boite.

        params:
          - direction: [int] quel point utiliser

        return: (x, y) les coordonnées du coin de la boite.
        """
        if direction == PdfObject.NW:
            return self.box[0], self.box[1]
        elif direction == PdfObject.NE:
            return self.box[2], self.box[1]
        elif direction == PdfObject.SW:
            return self.box[0], self.box[3]
        elif direction == PdfObject.SE:
            return self.box[2], self.box[3]
        return None, None



    def dist(self, obj, mydir=NW, hisdir=NW):
        """
        Calcule la distance entre deux boites en prenant pour référence les coins indiqués
        La distance est calculée:
            10000 abs(p_2-p_1) + sqrt( (x_2-x_1)^2 + (y_2-y_1)^2 )

        params:
          - obj: [PdfObject] le deuxième objet pour calculer la distance
          - mydir: [int] le point de référence de self
          - hisdir: [int] le point de référence de obj

        return: la distance entre les boites
        """
        mx, my = self.get_point(mydir)
        hx, hy = obj.get_point(hisdir)
        mp, hp = self.pno, obj.pno
        return 10000*abs(mp-hp) + sqrt((mx-hx)**2 + (my-hy)**2)









class PdfText(PdfObject):
    """
    Objet représentant du texte.
    """

    def __init__(self, data, pno):
        """
        Initialise l'objet

        params:
          - data: [dict] les données directement issue du pdf
          - pno: [int] numéro de page de l'objet
        """
        super().__init__(data["bbox"], pno)
        self.text = ""
        for ks, span in enumerate(data["spans"]):
            self.text += (" " if ks > 0 else "")
            self.text += span["text"]









class PdfImage(PdfObject):
    """
    Objet représentant une image.
    """

    def __init__(self, data, pno):
        """
        Initialise l'objet

        params:
          - data: [dict] les données directement issue du pdf
          - pno: [int] numéro de page de l'objet
        """
        super().__init__(data["bbox"], pno)
        self.image = base64.b64decode(data["image"])
        self.nom = "image-" + str(pno) + "-" + str(data["number"]) + "." + data["ext"]
        self.chaise_vide = False









class PdfEleve:
    """
    Une représentation d'un élève, avec les données du pdf.
    """

    def __init__(self, pno=-1):
        """
        Initialise l'objet

        params:
          - pno: [int] numéro de page de l'objet
        """
        self.pno = pno
        self.text_element = None
        self.image_element = None



    def to_TRB(self, classe, dossier):
        """
        transforme l'objet en TrbEleve

        params:
          - classe: [TrbClasse] la classe de l'élève
          - dossier: [string] le dossier parent pour les images

        return: [TrbEleve] données sous forme de trb.
        """
        trb = TrbEleve(classe=classe)
        if self.text_element != None:
            nomprenom = self.text_element.text
            nom = " ".join(s for s in nomprenom.split() if s.upper() == s)
            prenom = " ".join(s for s in nomprenom.split() if s.upper() != s)
            trb.nom = [nom, prenom]
        if self.image_element != None:
            trb.photo = os.path.join(dossier, self.image_element.nom)
            trb.chaise_vide = self.image_element.chaise_vide
        return trb









class PdfClasse:
    """
    Une représentation d'une classe, avec les données du pdf.
    """

    def __init__(self, pno=-1):
        """
        Initialise l'objet

        params:
          - pno: [int] numéro de page de l'objet
        """
        self.pno = pno
        self.nom = None
        self.eleves = []



    def to_TRB_File(self, dossier_trb, dossier_photos, parametres_trb):
        """
        Conversion de l'objet en TrbClasse

        params:
          - dossier_trb: [string] dossier contenant le futur fichier trb
          - dossier_photos: [string] dossier contenant les photos de la classe
          - parametres_trb: [TrbClasse] paramètres de base du futur TrbClasse

        return: TrbClasse
        """
        trb = TrbClasse()
        trb.nom_classe = self.nom
        trb.load_trb(data=parametres_trb, basedir=dossier_trb)

        for ele in self.eleves:
            trbel = ele.to_TRB(classe=trb, dossier=dossier_photos)
            trb.eleves.append(trbel)
        trb.eleves.sort(key=lambda x:unidecode.unidecode(" ".join(x.nom)))
        return trb









class ExtractionManager(TaskManagerThread):
    """
    Classe d'extraction basée sur le Task Manager.
    L'extraction se fait donc dans un thread séparé.
    """

    def __init__(self, log_panel, wind):
        """
        initialise l'objet

        params:
          - log_panel: [TaskManagerPanel] le panel qui permet de log.
          - wind: la fenetre parent du GUI pour pouvoir afficher la fenetre de reglage des paramètres par défaut
        """
        super().__init__(log_panel)
        log_panel.set_manager(self)
        self.wind = wind
        self.start_manager()



    def print_step(self, nb, message):
        """
        Demande l'affichage de l'étape en cours.
        Incrémente le compteur d'étape.

        params:
          - nb: [int] compteur d'étape
          - message: [string] message à afficher

        return: [int] le nouveau compteur d'étape
        """
        self.log_panel.add_text("Étape " + str(nb) + ": " + message, prefix="\n##### ", is_important=True)
        return 1 + nb



    def etape_ouverture_fichier(self, ficname, logit=True):
        """
        Etape d'ouverture de fichier. Juste l'ouverture du fichier.

        params:
          - ficname: [string] chemin absolu du fichier trb à fichier_pdf_a_traiter
          - logit: [booléen] Doit on l'afficher dans le GUI ?

        return: [fitz.doc] le document pdf
        """
        try:
            doc = fitz.open(ficname)
        except Exception as e:
            self.log_message("/!\ Problème de lecture de fichier: " + str(e), logit)
            return None
        self.log_message(str(doc.page_count) + " pages trouvée" + ("s" if doc.page_count > 0 else ""), logit)
        return doc



    def etape_extraction(self, doc, logit=True):
        """
        Etape d'extraction des données du document pdf: image et texte.
        Le seul tri qui est fait est sur le type: les images d'un coté, les textes de l'autre.

        params:
          - doc: [fitz.doc] document pdf
          - logit: [booleén] Doit on l'afficher dans le GUI ?

        return [list] (textes, images) liste des textes et des images sous forme PdfText et PdfImage
        """
        textes, images = [], []
        for pno in range(doc.page_count):
            if self.is_stopped:
                return None, None
            page = json.loads(doc.load_page(pno).get_text('json'))
            self.log_message("Page " + str(1+pno) + " (" + str(len(page["blocks"])) + " objets)", logit)
            for bno, bloc in enumerate(page["blocks"]):
                if bloc["type"] == 0:
                    ### du texte
                    for line in bloc["lines"]:
                        obj = PdfText(line, pno)
                        self.log_message("Objet " + str(bno) + ": [Texte] " + obj.text.replace("\n"," "), logit)
                        textes.append(obj)
                elif bloc["type"] == 1:
                    ### une image
                    obj = PdfImage(bloc, pno)
                    self.log_message("Objet " + str(bno) + ": [Image] " + obj.nom, logit)
                    images.append(obj)
                else:
                    ### un truc inconnu
                    self.log_message("Objet " + str(bno) + ": [Inconnu] type " + str(bloc["type"]) + ")", logit)

            self.log_message("", logit)
        return textes, images



    def etape_selection_textes(self, textes, logit=True):
        """
        Etape de sélection dans les textes.
        On suprime ce qui n'est pas pertinent.
        On garde le nom de la classe pour l'association plus tard.
        Ne renvoie rien, la liste est mise à jour en place.

        params:
          - textes: [list] liste de PdfText
          - logit: [booléen] Doit on l'afficher dans le GUI ?

        return: None
        """
        ind = 0
        while ind < len(textes):
            obj = textes[ind]
            onjette = ("Trombino" in obj.text) or ("GASPARD MONGE" in obj.text) or ("Index Education" in obj.text) or ("USAGE INTERNE" in obj.text)
            if onjette:
                textes.pop(ind)
                self.log_message("Page " + str(obj.pno) + ", suppression de «" + obj.text.replace("\n", " ") + "»", logit)
            else:
                ind += 1
                """
                if obj.text.count("\n") == 0:
                    obj.text += "\n" + textes[ind].text
                    textes.pop(ind)
                    self.log_message("Page " + str(obj.pno) + ", concaténation de «" + obj.text.replace("\n", " ") + "»", logit)
                """
                obj.text = obj.text.replace("--", "-").replace(" - ", "-")



    def etape_selection_images(self, images, logit=True):
        """
        Etape de sélection des images.
        Tente de deviner si l'image est une chaise vide. Deux critères:
          - Si l'image est trop petite,
          - Si le centre de l'image est sensiblement de la même couleur qu'un fond prédeterminé
        Ne renvoie rien, la liste est mise à jour en place.

        params:
          - images: [list] liste de PdfImage
          - logit: [booléen] Doit on l'afficher dans le GUI ?

        return: None
        """
        for pdf_img in images:
            if self.is_stopped:
                return
            img = Image.open(BytesIO(pdf_img.image))

            if img.size[0] > 100:
                # tentative de découverte de photo de chaise vide:
                # on calcul la moyenne des écarts entre une couleur standard de chaise vide
                # et nos pixels sur une partie de l'image.
                # Les valeurs ont été calculées au doigt mouillé
                moyenne, nb_pix = 0, 0
                for x in range(45,75):
                    for y in range(40,60):
                        r, g, b = img.getpixel((x,y))
                        moyenne += abs(r-132) + abs(g-136) + abs(b-143)
                        nb_pix += 1
                moyenne = moyenne/nb_pix
                pdf_img.chaise_vide = moyenne < 20
                if pdf_img.chaise_vide:
                    self.log_message(pdf_img.nom + ": chaise vide (moyenne des couleurs)", logit)
            else:
                pdf_img.chaise_vide = True
                self.log_message(pdf_img.nom + ": chaise vide (image trop petite)", logit)



    def etape_association_images(self, textes, images, logit=True):
        """
        Etape d'association texte <-> image
        A chaque texte, on essaye de faire correspondre une image.
        A priori, certains textes n'ont pas d'image mais pas le contraire
        (des images sans texte).
        Les images non associées sont couplées avec un nom vide.

        params:
          - textes: [list] de PdfText contenant les noms des élèves
          - images: [list] de PdfImage
          - logit: [booléen] Doit on l'afficher dans le GUI ?

        return: [list] de PdfEleve
        """
        eleves = []
        associated = [False]*len(images)
        for obj in textes:
            if self.is_stopped:
                return
            eleve = PdfEleve(obj.pno)
            eleve.text_element = obj
            ind = 0
            while ind < len(images):
                if (not associated[ind]) and (obj.dist(images[ind], mydir=PdfObject.NW, hisdir=PdfObject.SW) < 25):
                    eleve.image_element = images[ind]
                    associated[ind] = True
                    break
                ind += 1
            self.log_message(obj.text + " --> " + (eleve.image_element.nom if eleve.image_element != None else "????"), logit)
            eleves.append(eleve)
        for k in range(len(images)):
            if not associated[k]:
                eleve = PdfEleve(images[k].pno)
                eleve.image_element = images[k]
                self.log_message("???? --> " + images[k].nom, logit)
        return eleves



    def etape_association_classes(self, eleves, logit=True):
        """
        Etape d'association eleves <-> classe
        une page par classe. On doit donc récupérer le texte "Élèves"

        params:
          - eleves: [list] de PdfEleve
          - logit: [booléen] Doit on l'afficher dans le GUI ?

        return: [list] de PdfClasse
        """
        classes = []
        dico = {}
        ind = 0
        while ind < len(eleves):
            if self.is_stopped:
                return
            if " de la classe" in eleves[ind].text_element.text:
                eleve = eleves[ind]
                classe = PdfClasse(eleve.pno)
                classe.nom = eleve.text_element.text[20:]
                dico[classe.pno] = classe
                classes.append(classe)
                eleves.pop(ind)
                self.log_message("Nouvelle classe: " + classe.nom, logit)
            else:
                ind += 1

        for eleve in eleves:
            if self.is_stopped:
                return
            classe = dico[eleve.pno]
            classe.eleves.append(eleve)
            self.log_message(eleve.text_element.text + ": classe " + classe.nom, logit)
        if logit:
            for cla in classes:
                self.log_message(cla.nom + ": " + str(len(cla.eleves)) + " élèves", logit)
        return classes



    def etape_sauver_images(self, images, img_dest, logit=True):
        """
        Etape d'enregistrement des images.
        On crée le dossier de destination si il n'existe pas.
        ATTENTION: on écrase d'éventuelles images déjà présentes. Aucune vérification n'est faite.

        params:
          - images: [list] de PdfImage
          - img_dest: [string] dossier dans lequel enregistrer les images
          - logit: [booléen] Doit on l'afficher dans le GUI ?

        return: [booléen] True si tout s'est bien passé, False si une erreur s'est produite ou si le processus a été stoppé
        """
        if os.path.exists(img_dest):
            if os.path.isfile(img_dest):
                self.log_message("/!\ La destination est un fichier. On attendait un répertoire.")
                return False
        else:
            try:
                os.mkdir(img_dest)
            except Exception as e:
                self.log_message("Impossible de créer la destination des images.")
                return False
        for img in images:
            if self.is_stopped:
                return False
            fic = os.path.join(img_dest, img.nom)
            try:
                with open(fic, 'wb') as ifile:
                    ifile.write(img.image)
                self.log_message("image " + img.nom + " sauvée.", logit)
            except Exception as e:
                self.log_message("Erreur de sauvegarde pour l'image " + img.nom + ": " + str(e), logit)
        return True



    def etape_sauver_trb(self, classes, dest_trb, dest_img, logit=True):
        """
        Etape d'enregistrement de fichier trb.
        Dans cette étape, on transforme les chemins absolus des images en chemins relatifs
        par rapport au fichier trb.
        Ceci pour être moins rigide et pouvoir être transférer sur un autre ordi sans trop de problème.
        ATTENTION: on écrase d'éventuelles images déjà présentes. Aucune vérification n'est faite.

        params:
          - classes: [list] de PdfClasse
          - dest_trb: [string] dossier de destination du fichier trb
          - dest_img: [string] dossier de destination des images
          - logit: [booléen] Doit on l'afficher dans le GUI ?

        return: [booléen] True si tout s'est bien passé, False si une erreur s'est produite ou si le processus a été stoppé
        """
        if os.path.exists(dest_trb):
            if os.path.isfile(dest_trb):
                self.log_message("/!\ La destination est un fichier. On attendait un répertoire.")
                return False
        else:
            try:
                os.mkdir(dest_trb)
            except Exception as e:
                self.log_message("Impossible de créer la destination des données.")
                return False

        params_trb = TrbClasse()
        if params_trb == None:
            return False
        for classe in classes:
            if self.is_stopped:
                return False
            f_trb = classe.to_TRB_File(dest_trb, dest_img, params_trb)

            ind = 0
            while ind < len(f_trb.eleves):
                eleve = f_trb.eleves[ind]
                if len(eleve.nom) == 0 and len(eleve.photo) == 0:
                    # on enlève les (nom vide et photo vide)
                    f_trb.eleves.pop(ind)
                    self.log_message("Suppression d'un élève vide (classe " + classe.nom + ")", logit)
                elif ind > 0 and " ".join(eleve.nom) == " ".join(f_trb.eleves[ind-1].nom):
                    # on enlève les doublons
                    f_trb.eleves.pop(ind)
                    self.log_message("Suppression d'un doublon (classe " + classe.nom + "): " + " ".join(eleve.nom), logit)
                else:
                    ind += 1

            try:
                f_trb.sauve()
                self.log_message("Données sauvées: classe " + classe.nom, logit)
            except Exception as e:
                self.log_message("Erreur de sauvegarde pour la classe " + classe.nom + ": " + str(e), logit, cli_log=True)
        return True



    def manage_the_task(self, data):
        """
        Gère l'execution des étapes.
        La tache est un couple constitué d'un fichier pdf et d'un dossier de destination.
        La destination est le répertoire où seront stockés les fichiers trb.
        Les images seront dans le dossier `destination/Photos`.
        Les dossiers sont crées au besoin.

        params:
          - data: la tache à accomplir.

        return: [booléen] True si tout s'est bien passé, False si une erreur ou si le processus a été stoppé
        """
        fichier_pdf, destination = data

        netape = 1
        if self.is_stopped:
            return False

        self.log_message("Fichier: " + fichier_pdf)
        self.log_message("Dossier de reception: " + destination)

        netape = self.print_step(netape, "Ouverture du fichier...")
        doc = self.etape_ouverture_fichier(fichier_pdf)
        if doc == None:
            return False

        if self.is_stopped:
            doc.close()
            return False
        netape = self.print_step(netape, "Extraction des objets...")
        textes, images = self.etape_extraction(doc)
        doc.close()

        if self.is_stopped:
            return False
        netape = self.print_step(netape, "Tri du texte...")
        self.etape_selection_textes(textes)

        if self.is_stopped:
            return False
        netape = self.print_step(netape, "Détection des chaises vides")
        self.etape_selection_images(images)

        if self.is_stopped:
            return False
        netape = self.print_step(netape, "Association nom/image")
        eleves = self.etape_association_images(textes, images)

        if self.is_stopped:
            return False
        netape = self.print_step(netape, "Association nom/classe")
        classes = self.etape_association_classes(eleves)

        img_dest = os.path.join(destination, "Photos")

        if self.is_stopped:
            return False
        netape = self.print_step(netape, "Sauvegarde des données TRB")
        res = self.etape_sauver_trb(classes, destination, img_dest)
        if not res:
            return False

        if self.is_stopped:
            return False
        netape = self.print_step(netape, "Sauvegarde des images")
        res = self.etape_sauver_images(images, img_dest)
        if not res:
            return False

        return True














if __name__ == "__main__":
    import sys

    wind = tk.Tk()
    wind.title("Extraction des trombis")

    panneau = ExtractionPanel(wind)
    panneau.pack()

    if len(sys.argv) == 3:
        panneau.extract(sys.argv[1], sys.argv[2])

        wind.mainloop()

    else:
        print("2 arguments attendus: fichier_pdf_a_traiter et dossier_de_reception_des_données")


#
#
# END
#
#
