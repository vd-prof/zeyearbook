import os

import tkinter as tk
from tkinter import ttk

from tkinter import filedialog as fd

from trombi_extract import ExtractionPanel
from trombi_trb import TrbClasse
from trombi_page import PanneauPage
from trombi_modify import ModificationPanel
from trombi_generate import GenerationPanel




class MainTrombi(tk.Tk):
    """
    Classe principale du trombi.
    Permet de choisir les actions (extraction, traitement par lot, traitement individuel, génération)
    et de choisir les paramètres.
    """

    def __init__(self):
        """
        Initialise l'objet
        """
        super().__init__()

        self.title("Trombi - ZeYearBook")

        self.step_pan = StepPanel(self)
        self.params_pan = ParamsPanel(self)
        sep = ttk.Separator(self, orient='vertical')

        self.step_pan.pack(side=tk.LEFT, fill=tk.Y, expand=True)
        sep.pack(side=tk.LEFT, fill=tk.Y, pady=5)
        self.params_pan.pack(side=tk.RIGHT, fill=tk.Y, expand=True, padx=10)



    def update_buttons(self, status):
        """
        Demande la mise à jour l'état des boutons en fonction des paramètres.

        params:
          - status: [int] etat des boutons
        """
        self.step_pan.set_active(status)



    def do_extraction(self):
        """
        Lance l'extraction
        """
        source = self.params_pan.get_source()
        work = self.params_pan.get_working_directory()
        modal = tk.Toplevel(self)
        modal.title("Extraction - ZeYearBook")
        extra = ExtractionPanel(modal)
        extra.pack()
        modal.grab_set()
        extra.extract(source, work)



    def do_mass_job(self):
        """
        Lance le traitement par lot
        """
        work = self.params_pan.get_working_directory()
        modal = tk.Toplevel(self)
        modal.title("Modification globale - ZeYearBook")
        glob = MassParameterPanel(modal, work)
        glob.pack()
        modal.grab_set()



    def do_unit_job(self):
        """
        Lance le traitement individuel
        """
        work = self.params_pan.get_working_directory()
        modal = tk.Toplevel(self)
        modal.title("Modification par classe - ZeYearBook")
        indiv = ModificationPanel(modal)
        indiv.pack()
        modal.grab_set()
        trbfiles = [os.path.abspath(os.path.join(work, fic)) for fic in os.listdir(work) if fic.endswith(".trb")]
        indiv.set_trb_list(trbfiles)



    def do_generation(self):
        """
        Lance la génération
        """
        work = self.params_pan.get_working_directory()
        final = self.params_pan.get_final_directory()
        modal = tk.Toplevel(self)
        modal.title("Generation - ZeYearBook")
        gegene = GenerationPanel(modal)
        gegene.pack()
        modal.grab_set()
        trbfiles = [os.path.abspath(os.path.join(work, fic)) for fic in os.listdir(work) if fic.endswith(".trb")]
        gegene.set_pdf_directory(final)
        gegene.populate_files(trbfiles)









class StepPanel(tk.Frame):
    """
    Panneau qui contient les boutons d'action.
      - Extraction à partir d'un fichier
      - Traitement par lot
      - traitement individuel des fichiers
      - génération du pdf

    Transmet les ordres d'action à son parent
    """

    """ Rien n'est défini. tout doit être désactivé. """
    ACTIV_NONE = 0

    """ La source est bien défini. """
    ACTIV_SOURCE = 1

    """ Le dossier de travail est bien défini. """
    ACTIV_TRAVAIL = 2

    """ le dossier final est bien défini. """
    ACTIV_FINAL = 4


    def __init__(self, parent):
        """
        Initialise l'objet.

        params:
          - parent: [MainTrombi] parent du GUI
        """
        super().__init__(parent)
        self.parent = parent

        self.extrabut = tk.Button(self, width=23, text="Extraction", command=self.parent.do_extraction)
        self.tplotbut = tk.Button(self, width=23, text="Paramètres généraux", command=self.parent.do_mass_job)
        self.indivbut = tk.Button(self, width=23, text="Paramètres individuels", command=self.parent.do_unit_job)
        self.generbut = tk.Button(self, width=23, text="Génération", command=self.parent.do_generation)

        self.extrabut.pack(side=tk.TOP, pady=(80,40), padx=60)
        self.tplotbut.pack(side=tk.TOP, pady=40, padx=60)
        self.indivbut.pack(side=tk.TOP, pady=40, padx=60)
        self.generbut.pack(side=tk.TOP, pady=(40,80), padx=60)

        self.set_active(StepPanel.ACTIV_NONE)



    def set_active(self, state):
        """
        Met à jour l'état des boutons en fonction de ce qui est correctement défini.

        params:
          - state: [int] somme de ACTIV_*
        """
        self.extrabut['state'] = tk.DISABLED
        self.tplotbut['state'] = tk.DISABLED
        self.indivbut['state'] = tk.DISABLED
        self.generbut['state'] = tk.DISABLED
        if (state & StepPanel.ACTIV_SOURCE > 0) and (state & StepPanel.ACTIV_TRAVAIL > 0):
            self.extrabut['state'] = tk.NORMAL
        if state & StepPanel.ACTIV_TRAVAIL:
            self.tplotbut['state'] = tk.NORMAL
            self.indivbut['state'] = tk.NORMAL
        if (state & StepPanel.ACTIV_TRAVAIL > 0) and (state & StepPanel.ACTIV_FINAL > 0):
            self.generbut['state'] = tk.NORMAL









class SingleParameterPanelTrombi(tk.LabelFrame):
    """
    Frame gérant un paramètre d'execution.
    Elle est constituée d'un titre,
    un label pour la valeur du paramètre,
    un bouton pour le changer et
    un petit texte explicatif.

    Gère complètement ce paramètre.
    """

    def __init__(self, parent, title_text, help_text, get_pdf_file):
        """
        Initialise l'objet

        params:
          - parent: [ParamsPanel] parent du GUI
          - title_text: [string] text à afficher en tant que titre
          - help_text: [string] message à caractère informatif
          - get_pdf_file: [booléen] doit-on récupérer un fichier (True) ou un dossier (False)
        """
        super().__init__(parent, text=title_text, labelanchor=tk.NW)

        self.parent = parent
        self.pdf_file = get_pdf_file
        self.titre = title_text
        self.value = None

        self.label_nom = tk.Label(self, text="---")
        self.button = tk.Button(self, text="Parcourir", command=self.clic_button)
        labelH = tk.Label(self, text=help_text, font=('Helvetica 10'))

        self.label_nom.pack(side=tk.TOP)
        self.button.pack(side=tk.TOP)
        labelH.pack(side=tk.TOP, fill=tk.Y, expand=True)



    def clic_button(self):
        """
        Gestion du clic: ouvre un dialog pour sélectionner un fichier pdf ou un dossier.
        """
        if self.pdf_file:
            name = fd.askopenfilename(initialdir=".", title=self.titre, filetypes=(("fichier PDF", "*.pdf"),))
        else:
            name = fd.askdirectory(initialdir=".", title=self.titre)
        if len(name) > 0 and self.value != name:
            self.value = name
            val = "---" if name == None or len(name) == 0 else os.path.basename(name)
            if len(val) > 20:
                val = val[0:17] + "..."
            self.label_nom['text'] = val
            self.parent.update_status()



    def is_well_defined(self):
        """
        return True si le paramètre est définie correctement.
        """
        return self.value != None



    def get_parameter(self):
        """
        retourne la valeur du paramètre s'il est correctement défini ou None sinon
        """
        return self.value









class ParamsPanel(tk.Frame):
    """
    Frame contenant les différents paramètres d'execution
      - fichier pdf d'origine
      - dossier de travail
      - dossier final
    """

    def __init__(self, parent):
        """
        Initialise l'objet

        params:
          - parant: [MainTrombi] parent du GUI
        """
        super().__init__(parent)

        self.parent = parent

        self.source_pan = SingleParameterPanelTrombi(self, "Fichier source",
                                                     "fichier dont on doit extraire les données",
                                                     get_pdf_file=True)
        self.travail_pan = SingleParameterPanelTrombi(self, "Dossier de travail",
                                                     "dossier dans lequel on stocke\nles fichiers temporaires (images, trb)",
                                                      get_pdf_file=False)
        self.final_pan = SingleParameterPanelTrombi(self, "Dossier destination",
                                                     "dossier dans lequel seront stockés\nles fichiers pdf produits",
                                                     get_pdf_file=False)

        self.source_pan.pack(side=tk.TOP, fill=tk.BOTH, expand=True, ipadx=30, padx=10, pady=(20,10))
        self.travail_pan.pack(side=tk.TOP, fill=tk.BOTH, expand=True, ipadx=30, padx=10, pady=10)
        self.final_pan.pack(side=tk.TOP, fill=tk.BOTH, expand=True, ipadx=30, padx=10, pady=(10,20))



    def update_status(self):
        """
        update les boutons du MainTrombi en fonction des valeurs des paramètres.
        """
        stat = StepPanel.ACTIV_NONE
        if self.source_pan.is_well_defined():
            stat = stat | StepPanel.ACTIV_SOURCE
        if self.travail_pan.is_well_defined():
            stat = stat | StepPanel.ACTIV_TRAVAIL
        if self.final_pan.is_well_defined():
            stat = stat | StepPanel.ACTIV_FINAL
        return self.parent.update_buttons(stat)



    def get_source(self):
        """
        return le fichier source à extraire ou None s'il n'est pas défini
        """
        return self.source_pan.get_parameter()



    def get_working_directory(self):
        """
        return le dossier de travail ou None s'il n'est pas défini
        """
        return self.travail_pan.get_parameter()



    def get_final_directory(self):
        """
        return le dossier final dans lequel les pdf seront générés ou None s'il n'est pas défini
        """
        return self.final_pan.get_parameter()









class MassParameterPanel(tk.Frame):
    """
    Paneau contenant le Panneau de configuration de la page ainsi que 2 boutons
    pour annuler ou valiser les changements
    """

    def __init__(self, parent, work):
        """
        Initialise l'objet

        params:
          - parent: parent du gui
          - work: [string] dossier de travail
        """
        super().__init__(parent)
        self.parent = parent

        self.okbut = tk.Button(self, text="Valider", command=self.click_ok)
        self.cancelbut = tk.Button(self, text="Annuler", command=self.click_cancel)

        self.page = PanneauPage(self, real_data=False)

        self.page.pack(side=tk.BOTTOM, fill=tk.X)
        self.okbut.pack(side=tk.RIGHT, fill=tk.X)
        self.cancelbut.pack(side=tk.LEFT)

        self.classe = TrbClasse()
        self.page.load_trb(self.classe)
        self.trbfiles = [os.path.abspath(os.path.join(work, fic)) for fic in os.listdir(work) if fic.endswith(".trb")]



    def click_ok(self):
        """
        gestion du clic sur le bouton ok:
        on sauve les paramètres chez tous les trb de la liste
        """
        self.page.save_trb()
        for trb in self.trbfiles:
            infic = TrbClasse(ficname=trb)
            outfic = TrbClasse(trb_data=self.classe)

            outfic.fichier = infic.fichier
            outfic.nom_classe = infic.nom_classe
            outfic.eleves = infic.eleves
            outfic.sauve()
        self.parent.destroy()



    def click_cancel(self):
        """
        gestion du clic sur le bouton annuler
        """
        self.parent.destroy()














if __name__ == "__main__":
    wind = MainTrombi()
    wind.mainloop()
