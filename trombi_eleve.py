import tkinter as tk
from tkinter import ttk

from tkinter.simpledialog import askinteger
from tkinter.messagebox import askyesno

from PIL import Image, ImageTk

from trombi_trb import *
from trombi_gui_lib import *



class PanneauCommandesEleve(ttk.Frame):
    """
    Sous-panneau de gestion des options de suppression et déplacement d'un élève

      - déplacement
      - suppression
    """

    def __init__(self, parent): #, page_panel, trb, eleve):
        """
        Initialise l'objet

        params:
          - parent: [PanneauEleve] parent du GUI
        """
        super().__init__(parent)
        self.parent = parent

        self.rmbut = ttk.Button(self, text="Supprimer", command=self.remove_me)
        self.mvbut = ttk.Button(self, text="Déplacer", command=self.move_me)
        self.rmbut.pack(side=tk.RIGHT)
        self.mvbut.pack(side=tk.RIGHT)
        self.rmbut["state"] = tk.DISABLED
        self.mvbut["state"] = tk.DISABLED



    def load_eleve(self, eleve):
        """
        Charge les données d'un élève.
        Ici, si eleve == None, on désactive les boutons, sinon, on les rend disponibles.

        return None
        """
        self.eleve = eleve
        state = tk.NORMAL if eleve != None else tk.DISABLED
        self.rmbut["state"] = state
        self.mvbut["state"] = state
        if eleve != None:
            self.trb = eleve.classe



    def save_trb(self):
        """
        Sauve les données dans le trb élève.
        Ici, rien à faire de particuler.

        return: True si on a modifié des choses dans le trb
        """
        return False



    def move_me(self):
        """
        Gestion du clic pour déplacement d'un élève.
        On affiche les numéros, on demande lequel est bon, on efface les numeros et on fait le changement.
        Enfin, on demande le redraw de la page.

        return: None
        """
        self.parent.page_panel.show_numbers(True)
        resp = askinteger("Nouvelle place", "Veuillez indiquer la nouvelle place de cet élève.", minvalue=1, maxvalue=len(self.trb.eleves))
        self.parent.page_panel.show_numbers(False)
        if resp != None:
            self.trb.eleves.remove(self.eleve)
            self.trb.eleves.insert(resp-1, self.eleve)
            self.parent.page_panel.redrawn_needed()



    def remove_me(self):
        """
        Gestion du clic pour suppression d'un élève
        Demande une confirmation avant.
        Demande le redraw de la page et des infos.

        return: None
        """
        resp = askyesno(title="Confirmation", message="Voulez-vous vraiment effacer cet élève ?")
        if resp:
            self.trb.eleves.remove(self.eleve)
            self.parent.page_panel.redrawn_needed()
            self.parent.page_panel.reload_trb()









class PanneauPhotoEleve(AbstractLabelFrame):
    """
    Sous-panneau de gestion des options de photo d'un élève

      - affichage de l'image d'origine (si disponible)
      - est-ce une chaise vide ?
    """

    def __init__(self, parent):
        """
        Initialise l'objet

        params:
          - parent: [PanneauEleve] parent du GUI
        """
        super().__init__(parent, "Photo", parent.page_panel)

        self.check_value = tk.BooleanVar()
        self.check = tk.Checkbutton(self, text="Chaise vide", variable=self.check_value, onvalue=True, offvalue=False, command=self.action_needed)
        self.check.pack(side=tk.TOP)

        self.larg, self.haut = 128, 128 # dimension maximales de l'image à afficher
        self.canvas = tk.Canvas(self, width=self.larg, height=self.haut)
        self.canvas.pack(side=tk.BOTTOM, pady=10)
        self.photo = None

        self.load_eleve(None)



    def load_eleve(self, eleve):
        """
        Charge les données d'un élève.
        """
        self.eleve = eleve
        if self.eleve != None:
            self.check_value.set(self.eleve.chaise_vide)
        self.check['state'] = tk.NORMAL if self.eleve != None and len(self.eleve.photo) > 0 else tk.DISABLED

        self.canvas.delete("all")
        self.photo = None
        if self.eleve != None and len(self.eleve.photo) > 0:
            image = Image.open(self.eleve.photo)
            image.thumbnail( (self.larg, self.haut) )
            self.photo = ImageTk.PhotoImage(image)
            self.canvas.create_image(self.larg/2, self.haut/2, image=self.photo, anchor=tk.CENTER)
        else:
            self.canvas.create_rectangle(1, 1, self.larg, self.haut, outline="black")



    def save_trb(self):
        """
        Gestion du clic sur la checkbox.

        return: True si l'état a été modifié
        """
        if self.eleve == None:
            return False
        newval = self.check_value.get()
        if newval != self.eleve.chaise_vide:
            self.eleve.chaise_vide = newval
            return True
        return False









class PanneauNomEleve(AbstractLabelFrame):
    """
    Sous-panneau de gestion des options de nom d'un élève.

      - 3 entries pour les 3 lignes possibles.
    """

    def __init__(self, parent):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
        """
        super().__init__(parent, "Nom", parent.page_panel)

        self.values = []
        self.entries = []
        for k in [0,1,2]:
            value = tk.StringVar()
            self.values.append(value)
            entry = tk.Entry(self, width=15, textvariable=value)
            entry.bind('<FocusOut>', lambda x:self.action_needed())
            entry.bind('<Return>', lambda x:self.action_needed())
            entry.bind('<KP_Enter>', lambda x:self.action_needed())
            entry.pack(side=tk.TOP, pady=5)
            self.entries.append(entry)

        self.load_eleve(None)



    def load_eleve(self, eleve):
        """
        Charge les données de l'élève.

        params:
          - eleve: [TrbEleve | None]
        """
        self.eleve = eleve
        for v, e in zip(self.values, self.entries):
            v.set("")
            e["state"] = tk.NORMAL if eleve != None else tk.DISABLED

        if eleve != None:
            for nom, val in zip(eleve.nom, self.values):
                val.set(nom)



    def save_trb(self):
        """
        Sauve les données dans le trb-élève

        return: True si quelque chose a été modifié
        """
        if self.eleve == None:
            return False
        values = list(map(lambda x:x.get(), self.values))
        while len(values) > 0 and len(values[-1]) == 0:
            values.pop()
        if str(self.eleve.nom) != str(values):
            self.eleve.nom = values
            return True
        return False









class PanneauEleve(tk.LabelFrame):
    """
    Panneau de gestion de toutes les options d'un élève.

    Charge les sous-panneaux adéquats (nom, photo, supprimer/deplacer, taille/nom font)
    et un bouton pour ajouter un élève.
    """

    def __init__(self, parent):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
          - page_panel: [PanneauPage] panneau représentant la page
        """
        super().__init__(parent, text="Options de l'élève", font=('Helvetica 16 bold'), labelanchor=tk.N)
        self.page_panel = parent

        self.addbut = ttk.Button(self, text="Ajouter", command=self.add_eleve)
        self.addbut.pack(side=tk.BOTTOM, pady=15)
        self.addbut["state"] = tk.DISABLED

        self.imgpan = PanneauPhotoEleve(self)
        self.nompan = PanneauNomEleve(self)
        self.cmdpan = PanneauCommandesEleve(self)

        self.imgpan.pack(fill=tk.X, padx=20, pady=10)
        self.nompan.pack(fill=tk.X, padx=20, pady=10)
        self.cmdpan.pack(fill=tk.X, padx=20, pady=10)

        self.classe = None



    def load_trb(self, trb):
        """
        Charge les données d'un TrbClasse
        """
        if trb != self.classe:
            self.load_eleve(None)
        self.classe = trb
        self.addbut["state"] = tk.NORMAL if trb != None else tk.DISABLED



    def load_eleve(self, eleve):
        """
        Charge les données trb de classe

        params:
          - trb: TrbEleve

        return: None
        """
        if eleve != None and eleve.classe != self.classe:
            self.load_trb(eleve.classe)
        self.imgpan.load_eleve(eleve)
        self.nompan.load_eleve(eleve)
        self.cmdpan.load_eleve(eleve)



    def save_trb(self):
        """
        Sauve les données trb de la classe

        return: True si quelque chose a été modifié
        """
        res = False
        for pan in (self.imgpan, self.nompan, self.cmdpan):
            if pan.save_trb():
                res = True
        return res



    def add_eleve(self):
        """
        Gestion du clic sur le bouton pour ajouteur un élève

        return: None
        """
        if self.trb != None:
            new_eleve = TrbEleve(classe=self.trb)
            self.trb.eleves.append(new_eleve)
            self.page_panel.reload_trb()
            self.page_panel.select_eleve(new_eleve)




#
#
# END
#
#
