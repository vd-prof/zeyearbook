import fitz

from PIL import Image, ImageTk
from math import ceil




class PdfConverter:
    """
    Convertisseur pour transformer un TrbClasse en une représentation pdf.
    Permet aussi d'afficher ou pas les boites réservées (marges, elèves,...) pour mieux
    les visualiser et mieux appréhender les éventuels changements à faire.
    """

    """ couleur des contours de la boite indiquant les marges de la page """
    marge_box_color = fitz.utils.getColor("orchid")

    """ couleur de la boite de nom de classe quand tout va bien """
    classe_box_color = fitz.utils.getColor("cyan")

    """ couleur de la boite de nom de classe si problème """
    classe_prb_color = fitz.utils.getColor("darkblue")

    """ couleur de la boite de nom d'élève quand tout va bien """
    text_box_color = fitz.utils.getColor("chartreuse")

    """ couleur de la boite de nom d'élève si problème """
    text_prb_color = fitz.utils.getColor("darkgreen")

    """ couleur de la boite de photo d'élève quand tout va bien """
    image_box_color = fitz.utils.getColor("tomato")

    """ couleur de la boite de photo d'élève si problème """
    image_prb_color = fitz.utils.getColor("darkred")

    """ opacité de boite quand tout va bien """
    light_opacity = 0.2

    """ opacité de boite si problème """
    dark_opacity = 0.8

    def __init__(self, trb, log=print):
        """
        Initialise l'objet

        params:
          - trb: [TrbClasse] les données à traiter
          - log: [fct] fonction qui permet d'afficher d'éventuelles erreurs
                 Cette fonction accepte 2 strings en paramètre: un titre et un commentaire.
        """
        self.trb = trb
        self.log = log

        self.doc = None
        self.page = None



    def get_pdf_document(self, show_boxes=False):
        """
        Convertie les données trb en fichier pdf.

        params:
          - show_boxes: [booléen] si True, on dessine les boites en plus.

        return: [fitz.document] le document pdf
        """
        self.convert(show_boxes)
        return self.doc



    def get_optimal_number(self, n, mini):
        """
        Donne le nombre optimal de colonnes/lignes sachant le nombre de lignes/colonnes et le nombre d'élèves

        params:
          - n: [int] nombre de lignes/colonnes fixées
          - mini: [int] nombre d'élèves à afficher

        return: le nombre optimal de colonnes/lignes
        """
        return max(1, ceil(mini/n))



    def get_optimal_array(self, a, b, c, d):
        """
        Optimisation de la surface d'une case élève.
        Si on a un tableau de x colonnes par y lignes, ça donne ceci:
        Largeur d'une case  = (largeur_page - 2 * marge_page - (x-1) marge_eleve) / x
        Hauteur d'une case  = (hauteur_page - 2 * marge_page - marge_titre - (y-1) marge_eleve) / y

        On peut mettre les deux sous la forme:
        large = b/x - a
        haut  = d/y - c

        On a alors un rectangle large*haut pour afficher une image.
        On optimise alors pour avoir une surface maximale occupée par les images.

        params:
          - a:
          - b:
          - c:
          - d:

        return: (nb_colones , nb_lignes) optimaux pour maximiser les surfaces des images
        """
        mini = len(self.trb.eleves)
        xyopti = (0, 0)
        sopti = 0
        for x in range(1, mini+1):
            y = self.get_optimal_number(x, mini)
            s = sum(eleve.get_image_area(b/x-a, d/y-c) for eleve in self.trb.eleves)
            if s == 0:
                s = (a-b/x)*(c-d/y)
            if s > sopti:
                xyopti = (y, x)
                sopti = s
        return xyopti



    def get_factor(self):
        """
        return: facteur d'échelle pour la page pdf.
        """
        if self.trb.unite == "mm":
            factor = 842/297
        else:
            self.log("Erreur", "unité inconnue: " + self.trb.unite)
            factor = 1
        return factor



    def init_conversion(self, width, height):
        """
        Initialise le document et la page.

        params:
          - width: largeur de la page
          - height: hauteur de la page

        return: None
        """
        self.doc = fitz.Document()
        self.page = self.doc.new_page(width=width, height=height)



    def draw_marge_box(self, xmin, ymin, xmax, ymax):
        """
        Dessine la boite des marges de la page.

        params:
          - (xmin, ymin): [float, float] point supérieur gauche dessinable
          - (xmax, ymax): [float, float] point inférieur droit dessinable

        return: None
        """
        rect = (xmin, ymin, xmax, ymax)
        self.page.draw_rect( rect, color=PdfConverter.marge_box_color)



    def draw_nom_classe_box(self, xmin, ymin, xmax, ymax, showbox=False):
        """
        Affiche le nom de la classe.
        Si showbox == True, on affiche la boite réversée au nom.
            couleur claire si tout va bien,
            couleur foncé si la boite est trop petite,
            boite barrée si le nom de la classe est vide.

        params:
          - (xmin, ymin): [float, float] point supérieur gauche de la boite
          - (xmax, ymax): [float, float] point inférieur droit de la boite
          - showbox: [booléen] si True, on dessine la boite. Si False, on n'affiche que le nom de la classe.

        return: None
        """
        rect = (xmin, ymin, xmax, ymax)
        res = self.page.insert_textbox(rect, self.trb.nom_classe,
                                       fontname=self.trb.nom_font_classe,
                                       fontsize=self.trb.taille_font_classe)
        # Si res < 0, la boite est trop petite pour afficher le texte. Sinon, res >= 0.

        if showbox:
            ln = fitz.Font(self.trb.nom_font_classe).text_length(self.trb.nom_classe, self.trb.taille_font_classe)
            probleme = res < 0 or ln > xmax-xmin
            color = PdfConverter.classe_prb_color if probleme else PdfConverter.classe_box_color
            opacity = PdfConverter.dark_opacity if probleme else PdfConverter.light_opacity
            self.page.draw_rect(rect, color=color, fill=color, fill_opacity=opacity, dashes="[5] 0")
            if len(self.trb.nom_classe) == 0:
                self.page.draw_line( (xmin, ymin), (xmax, ymax), color=color, width=5)
                self.page.draw_line( (xmin, ymax), (xmax, ymin), color=color, width=5)



    def draw_eleve_box(self, xmin, xmax, ymin, ymid, ymax, eleve, showbox=False):
        """
        Dessine tout ce qu'il faut de l'élève désiré dans la boite désignée: photo et nom.
        Si showbox == True, on affiche aussi les contours des boites.
        Boite de texte:
            couleur claire si tout va bien,
            couleur foncée si la boite est trop petite,
            boite barrée si rien à afficher (pas de nom)
        Boite de photo:
            couleur claire si tout va bien,
            couleur foncée si pas d'image-élève,
            boite barrée si la photo est considéeée comme une chaise vide

        Si showbox == True, on affiche l'image de l'élève quoi qu'il arrive.
        Si showbox == False, on affiche l'image de l'élève si ce n'est pas une chaise vide.
        sinon, on affiche l'image par défaut (si elle est demandée) ou rien (sinon)

        params:
          - (xmin, ymin): [float, float] point supérieur gauche de la boite
          - (xmax, ymax): [float, float] point inférieur droit de la boite
          - ymid: [float] hauteur de séparation entre photo et texte.
                  la ligne est entre les points (xmin, ymid) et (xmax, ymid).
          - eleve: [TrbEleve]
          - showbox: [booléen] si True, on dessine les boites en plus.

        return: None
        """
        ## texte
        rect = (xmin, ymid, xmax, ymax)
        res = self.page.insert_textbox(rect, eleve.nom,
                                       align=fitz.TEXT_ALIGN_CENTER,
                                       fontname=self.trb.nom_font_eleve,
                                       fontsize=self.trb.taille_font_eleve)
        # Si res < 0, la boite est trop petite pour afficher le texte. Sinon, res >= 0.

        if showbox:
            ft = fitz.Font(self.trb.nom_font_eleve)
            if len(eleve.nom) > 0:
                ln = max( ft.text_length(nom, self.trb.taille_font_eleve) for nom in eleve.nom)
                probleme = res < 0 or ln > xmax-xmin
            else:
                probleme = True
            color = PdfConverter.text_prb_color if probleme else PdfConverter.text_box_color
            opacity = PdfConverter.dark_opacity if probleme else PdfConverter.light_opacity
            self.page.draw_rect(rect, color=color, dashes="[5] 0", fill=color, fill_opacity=opacity)
            if len(eleve.nom) == 0:
                self.page.draw_line( (xmin, ymid), (xmax, ymax), color=color, width=5)
                self.page.draw_line( (xmin, ymax), (xmax, ymid), color=color, width=5)

        ## image
        rect = (xmin, ymin, xmax, ymid)
        image = None
        if showbox:
            if len(eleve.photo) > 0:
                image = eleve.photo
            if image != None:
                self.page.insert_image(rect, filename=image)
            color = PdfConverter.image_prb_color if image == None else PdfConverter.image_box_color
            opacity = PdfConverter.dark_opacity if image == None else PdfConverter.light_opacity
            self.page.draw_rect(rect, color=color, dashes="[5] 0", fill=color, fill_opacity=opacity)
            if eleve.chaise_vide:
                self.page.draw_line( (xmin, ymin), (xmax, ymid), color=color, width=5)
                self.page.draw_line( (xmin, ymid), (xmax, ymin), color=color, width=5)
        else:
            if eleve.chaise_vide:
                if self.trb.image_chaise_vide and len(self.trb.fichier_chaise_vide) > 0:
                    image = self.trb.fichier_chaise_vide
            elif len(eleve.photo) > 0:
                image = eleve.photo
            if image != None:
                self.page.insert_image(rect, filename=image)



    def convert(self, show_boxes=False):
        """
        Méthode principale de conversion.
        Elle calcule tout et appelle les autres méthodes avec les bons paramètres.

        params:
          - show_boxes: [booléen] si True, on dessine les boites en plus.

        return: None
        """
        factor = self.get_factor()

        paper_width = int(self.trb.largeur_page * factor)
        paper_height = int(self.trb.hauteur_page * factor)
        self.init_conversion(paper_width, paper_height)

        marge_width = self.trb.marge_horizontale_page * factor
        marge_height = self.trb.marge_verticale_page * factor
        if show_boxes:
            self.draw_marge_box(marge_width, marge_height, paper_width-marge_width, paper_height-marge_height)

        classe_name_height = 0
        if self.trb.affiche_nom_classe:
            classe_name_height = self.trb.hauteur_nom_classe * factor
            self.draw_nom_classe_box(marge_width, marge_height, paper_width-marge_width, marge_height+classe_name_height, show_boxes)

        inter_width = self.trb.marge_horizontale_eleves * factor
        inter_height = self.trb.marge_verticale_eleves * factor
        nb_eleve = len(self.trb.eleves)
        if nb_eleve > 0:
            ncol = self.trb.nb_colonnes
            nlig = self.trb.nb_lignes
            if ncol == 0 and nlig == 0:
                a = inter_width
                b = paper_width - 2 * marge_width + inter_width
                c = inter_height
                d = paper_height - 2 * marge_height - classe_name_height + inter_height
                ncol, nlig = self.get_optimal_array(a, b, c, d)
            elif ncol == 0:
                ncol = self.get_optimal_number(nlig, nb_eleve)
            elif nlig == 0:
                nlig = self.get_optimal_number(ncol, nb_eleve)
            elif ncol*nlig < nb_eleve:
                self.log("Attention", "Cette configuration ne permet pas d'afficher tous les élèves")

            # case élève: photo + 3 lignes de texte
            box_width = (paper_width - 2*marge_width - (ncol-1)*inter_width)/ncol
            box_height = (paper_height - 2*marge_height - (nlig-1)*inter_height - classe_name_height)/nlig

            img_width = box_width
            img_height = box_height - self.trb.hauteur_nom_eleve * factor

            dx = box_width + inter_width
            xmin = marge_width
            dy = box_height + inter_height
            ymin = marge_height + classe_name_height
            x, y = 0, 0
            for eleve in self.trb.eleves:
                x1 = xmin + x*dx
                x2 = x1 + box_width
                y1 = ymin + y*dy
                y2 = y1 + img_height
                y3 = y1 + box_height

                self.draw_eleve_box(x1, x2, y1, y2, y3, eleve, showbox=show_boxes)

                ## Update les coordonnées (x,y) pour le prochain élève.
                x += 1
                if x == ncol:
                    x = 0
                    y += 1









class GuiEleveBox:
    """
    Structure de données nécessaires pour se souvenir
    de l'endroit de la boite d'un élève dans la représentation de la page.

    La boite comprend l'espace pour la photo et le nom
    """

    def __init__(self, xmin, ymin, xmax, ymax, eleve):
        """
        Initialise l'objet

        params:
          - (xmin, ymin): point en haut à gauche de la boite
          - (xmax, ymax): point en bas à droite de la boite
          - eleve: trb de l'élève
        """
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.eleve = eleve



    def contains_point(self, x, y):
        """
        True si le point (x,y) est dans la boite de l'élève.
        """
        return self.xmin <= x and x <= self.xmax and self.ymin <= y and y <= self.ymax









class PixmapConverter(PdfConverter):
    """
    Convertie les données TRB d'une classe en image pixmap pour afficher dans un tk.canvas (par exemple).
    Ceci passe par génération d'une page pdf. Comme ça, l'image est cohérente avec le résultat final.

    On peut avoir accès aux boites-eleves, ce qui permet de coder une interaction avec l'image.
    """

    def __init__(self, trb, log):
        """
        Initialise l'objet

        params:
          - trb [TrbClasse] les données à traiter
          - log: [fct] fonction qui permet d'afficher d'éventuelles erreurs
                 Cette fonction accepte 2 strings en paramètre: un titre et un commentaire.
        """
        super().__init__(trb, log)
        self.boxes = []
        self.width = 0
        self.height = 0



    def get_tkImage(self, show_boxes=False):
        """
        Convertie les données en pixmap.

        params:
          - show_boxes: [booléen] si True, on dessine les boites en plus.

        return: [ImageTk] le pixmap des données.
        """
        doc = self.get_pdf_document(show_boxes)
        pix = doc.load_page(0).get_pixmap()
        mode = "RGBA" if pix.alpha else "RGB"
        img = Image.frombytes(mode, [pix.width, pix.height], pix.samples)
        return ImageTk.PhotoImage(img)



    def get_boxes(self):
        """
        Permet d'obtenir des boites elèves. Pour interaction.

        return: [list] liste de GuiEleveBox
        """
        return self.boxes



    def init_conversion(self, width, height):
        """
        surcharge de PdfConverter.init_conversion
        """
        super().init_conversion(width, height)
        self.width = width
        self.height = height



    def draw_eleve_box(self, xmin, xmax, ymin, ymid, ymax, eleve, showbox=False):
        """
        Surcharge de PdfConverter.draw_eleve_box
        Permet de stocker les boite-eleves
        """
        super().draw_eleve_box(xmin, xmax, ymin, ymid, ymax, eleve, showbox)
        self.boxes.append(GuiEleveBox(xmin, ymin, xmax, ymax, eleve))



#
#
# END
#
#
