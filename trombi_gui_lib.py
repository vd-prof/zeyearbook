import tkinter as tk
from tkinter import ttk

from trombi_trb import *




class AbstractLabelFrame(tk.LabelFrame):
    """
    Classe abstraite d'un panneau avec une étiquette et
    gestion basique du refresh de la représentation de la page.
    """

    def __init__(self, parent, texte, page_panel):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
          - texte: [string] texte du l'étiquette
          - page_panel: [PanneauPage] le panneau représentant la page.
        """
        super().__init__(parent, text=texte, font=('Helvetica 10 bold'), labelanchor=tk.NW)
        self.page_panel = page_panel



    def save_trb(self):
        """
        Méthode abstraite.
        Actions à réaliser en cas de demande de sauvegarde des données trb

        return: True si quelque chose a changé
        """
        return False



    def action_needed(self):
        """
        Demande au panneau représentant la page de s'actualiser.

        return: None
        """
        if self.save_trb():
            self.page_panel.redrawn_needed()









class GenericTwoValuesPanel(AbstractLabelFrame):
    """
    Classe générique qui gère deux valeurs modifiables.

    On peut préciser les étiquettes et les types de champs souhaités
    """

    def __init__(self, parent, page_panel, trb_field1, trb_field2,
                 label_text="label text",
                 label_entry1="label entry 1",
                 label_entry2="label entry 2",
                 type_field=int,
                 nb_char_displayed=5,
                 unit_display=True):
        """
        Initialise l'objet

        params:
          - parent: parent GUI
          - page_panel: [PanneauCanvas] panneau représentant la page
          - trb_field1: [string] nom du 1er champs dans la structure de données trb
          - trb_field2: [string] nom du 2ème champs dans la structure de données trb
          - label_text: [string] étiquette générale des champs
          - label_entry1: [string] étiquette du 1er champs
          - label_entry2: [string] étiquette du 2ème champs
          - type_field: [type] type des champs voulu
          - nb_char_displayed: [int] nombre de caractères affiché dans l'Entry
          _ unit_dispaly: [booléen] doit-on afficher l'unité de mesure ?
        """
        super().__init__(parent, label_text, page_panel)
        self.trb_field1 = trb_field1
        self.trb_field2 = trb_field2
        self.type_field = type_field

        ttk.Label(self, text=label_entry1).grid(row=0, column=0)

        self.old_val1 = 0
        self.entry1 = ttk.Entry(self, width=nb_char_displayed)
        self.entry1.insert(0, "0")
        self.entry1.grid(row=0, column=1, padx=4, pady=2)
        self.entry1.bind('<Return>', self.valide_entry)
        self.entry1.bind('<KP_Enter>', self.valide_entry)
        self.entry1.bind('<FocusOut>', self.valide_entry)

        ttk.Label(self, text=label_entry2).grid(row=1, column=0)

        self.old_val2 = 0
        self.entry2 = ttk.Entry(self, width=nb_char_displayed)
        self.entry2.insert(0, "0")
        self.entry2.grid(row=1, column=1, padx=4, pady=4)
        self.entry2.bind('<Return>', self.valide_entry)
        self.entry2.bind('<KP_Enter>', self.valide_entry)
        self.entry2.bind('<FocusOut>', self.valide_entry)

        self.unit_display = unit_display
        self.unit_label1 = ttk.Label(self, text="")
        self.unit_label1.grid(row=0, column=2)

        self.unit_label2 = ttk.Label(self, text="")
        self.unit_label2.grid(row=1, column=2)

        self.trb = None



    def save_trb(self):
        """
        Sauve la structure trb

        return: None
        """
        change = False
        if self.trb != None:
            if getattr(self.trb, self.trb_field1) != self.old_val1:
                setattr(self.trb, self.trb_field1, self.old_val1)
                change = True
            if getattr(self.trb, self.trb_field2) != self.old_val2:
                setattr(self.trb, self.trb_field2, self.old_val2)
                change = True
        return change



    def load_trb(self, trb):
        """
        Charge les données trb dans l'interface.

        params:
          - trb: [TrbClasse] données trb à traiter

        return: None
        """
        self.trb = trb
        self.old_val1 = getattr(self.trb, self.trb_field1)
        self.entry1.delete(0, tk.END)
        self.entry1.insert(0, str(self.old_val1))
        self.old_val2 = getattr(self.trb, self.trb_field2)
        self.entry2.delete(0, tk.END)
        self.entry2.insert(0, str(self.old_val2))
        if self.unit_display:
            self.unit_label1['text'] = "(" + self.trb.unite + ")"
            self.unit_label2['text'] = "(" + self.trb.unite + ")"



    def valide_entry(self, event):
        """
        Valide les entries.
        Si le type n'est pas bon, remet l'ancienne valeur.
        Si une valeur a changé, demande le refresh panneau de représentation.

        params:
          - event: l'event envoyé par le bind des Entries

        return: None
        """
        val1 = self.old_val1
        try:
            val1 = self.type_field(self.entry1.get())
        except:
            self.entry1.delete(0, tk.END)
            self.entry1.insert(0, str(val1))
        val2 = self.old_val2
        try:
            val2 = self.type_field(self.entry2.get())
        except:
            self.entry2.delete(0, tk.END)
            self.entry2.insert(0, str(val2))

        if self.old_val1 != val1 or self.old_val2 != val2:
            self.old_val1 = val1
            self.old_val2 = val2
            self.action_needed()









class FontPanel(ttk.Frame):
    """
    Panneau pour préciser le nom de la font, sa taille ainsi que la hauteur qu'il faut reserver.
    """

    @staticmethod
    def get_font_list():
        return ["helv", "heit", "hebo", "hebi", "cour", "coit", "cobo", "cobi", "tiro", "tibo", "tiit", "tibi", "symb", "zadb"]



    def __init__(self, parent, font_name_key, font_size_key, height_key):
        """
        params:
          - parent: [pagePanel]
          - font_name_key: [string] nom de l'attribut du nom de la font dans le trb
          - font_size_key: [string] nom de l'attribut de la taille de la font dans le trb
          - height_key: [string] nom de l'attribut de la taille réversé à la zone de texte dans le trb
        """
        super().__init__(parent)
        self.parent = parent

        self.height_value = tk.StringVar()
        self.size_value = tk.StringVar()

        self.old_height = "10"
        self.old_name = "helv"
        self.old_size = "10"

        self.height_key = height_key
        self.name_key = font_name_key
        self.size_key = font_size_key

        frame_up = ttk.Frame(self)
        frame_mid = ttk.Frame(self)
        frame_down = ttk.Frame(self)

        frame_up.pack()
        frame_mid.pack()
        frame_down.pack()

        self.height_entry = ttk.Entry(frame_up, textvariable=self.height_value, width=5)
        self.name_combobox = ttk.Combobox(frame_mid, width=7)
        self.size_entry = ttk.Entry(frame_down, textvariable=self.size_value, width=5)

        self.height_entry.bind('<Return>', self.valide_entry)
        self.height_entry.bind('<KP_Enter>', self.valide_entry)
        self.height_entry.bind('<FocusOut>', self.valide_entry)

        self.name_combobox.state(['readonly'])
        self.name_combobox.bind('<<ComboboxSelected>>', self.valide_entry)
        self.name_values = FontPanel.get_font_list()
        self.name_combobox['values'] = self.name_values

        self.size_entry.bind('<Return>', self.valide_entry)
        self.size_entry.bind('<KP_Enter>', self.valide_entry)
        self.size_entry.bind('<FocusOut>', self.valide_entry)

        self.height_label = tk.Label(frame_up, text="Hauteur réservée :")
        self.name_label = tk.Label(frame_mid, text="Nom de la police :")
        self.size_label = tk.Label(frame_down, text="Taille de la police :")

        self.height_unit = ttk.Label(frame_up, text="")
        self.size_unit = ttk.Label(frame_down, text="(pt)")

        self.height_entry.grid(row=0, column=1, padx=4, pady=4)
        self.name_combobox.grid(row=1, column=1, padx=4, pady=4)
        self.size_entry.grid(row=2, column=1, padx=4, pady=4)

        self.height_label.grid(row=0, column=0, padx=4, pady=4)
        self.name_label.grid(row=1, column=0, padx=4, pady=4)
        self.size_label.grid(row=2, column=0, padx=4, pady=4)

        self.height_unit.grid(row=0, column=2, padx=4, pady=4)
        self.size_unit.grid(row=2, column=2, padx=4, pady=4)



    def load_trb(self, trb):
        """
        Charge les données du trb.

        params:
          - trb: [trbClasse]
        """
        self.trb = trb
        self.old_height = getattr(self.trb, self.height_key)
        self.height_value.set(self.old_height)

        self.old_name = getattr(self.trb, self.name_key)
        ind = [k for k, v in enumerate(self.name_values) if v == self.old_name]
        ind = 0 if len(ind) == 0 else ind[0]
        self.name_combobox.current(ind)

        self.old_size = getattr(self.trb, self.size_key)
        self.size_value.set(str(self.old_size))

        self.height_unit['text'] = "(" + self.trb.unite + ")"



    def save_trb(self):
        """
        Sauve la structure trb

        return: True si on a modifié quelque chose
        """
        change = False
        if self.trb != None:
            if getattr(self.trb, self.height_key) != self.old_height:
                setattr(self.trb, self.height_key, self.old_height)
                change = True
            if getattr(self.trb, self.name_key) != self.old_name:
                setattr(self.trb, self.name_key, self.old_name)
                change = True
            if getattr(self.trb, self.size_key) != self.old_size:
                setattr(self.trb, self.size_key, self.old_size)
                change = True
        return change



    def set_state(self, state):
        """
        met à jour l'état des widget

        params:
          - state: tk.NORMAL ou tk.DISABLED
        """
        self.height_label["state"] = state
        self.height_entry["state"] = state
        self.name_label["state"] = state
        self.name_combobox["state"] = 'readonly' if state == tk.NORMAL else state
        self.size_label["state"] = state
        self.size_entry["state"] = state



    def valide_entry(self, event):
        """
        Valide les entries.
        Si le type n'est pas bon, remet l'ancienne valeur.
        Si une valeur a changé, demande le refresh panneau de représentation.

        params:
          - event: l'event envoyé par le bind des Entries

        return: None
        """
        valh = self.old_height
        try:
            valh = float(self.height_value.get())
        except:
            self.height_value.set(valh)

        valn = self.old_name
        try:
            valn = self.name_values[self.name_combobox.current()]
        except:
            self.name_value.set(valn)

        vals = self.old_size
        try:
            vals = int(self.size_value.get())
        except:
            self.size_value.set(vals)

        if self.old_height != valh or self.old_name != valn or self.old_size != vals:
            self.old_height = valh
            self.old_name = valn
            self.old_size = vals
            self.parent.action_needed()


#
#
# END
#
#
