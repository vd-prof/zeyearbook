import os, sys

import tkinter as tk
from tkinter import ttk

from trombi_trb import TrbClasse
from trombi_thread import TaskManagerThread, TaskManagerPanel
from trombi_converter import PdfConverter



class GenerationPanel(ttk.Frame):
    """
    Panneau général contenant toute l'interface de génération:
    la liste des fichiers disponibles,
    les boutons de sélection et déselection,
    les bontons de génération et d'arret.
    """

    def __init__(self, parent):
        """
        Initialise l'objet

        params:
          - parent: parent du gui
        """
        super().__init__(parent)

        self.panSel = GenerateSelectionPanel(parent)
        self.panGen = LogDisplayPanel(parent, self.panSel)

        self.panSel.pack(side=tk.LEFT)
        self.panGen.pack(side=tk.RIGHT)



    def set_pdf_directory(self, dir_gen):
        """
        dir_gen: [string] dossier où seront stockées les pdf générés
        """
        self.panSel.set_pdf_directory(dir_gen)



    def populate_files(self, liste):
        """
        met à jour la liste des fichiers trb disponibles

        params:
          - liste: [list] de string contenant chacune un chemin vers une fichier trb.

        return: None
        """
        liste.sort()
        self.panSel.populate_liste(liste)









class GenerateSelectionPanel(ttk.Frame):
    """
    Panneau qui permet de sélectionner les fichiers trb à convertir en pdf.
    Il s'agit d'une listbox avec un bouton de select-all, un autre de unselect-all.
    """

    def __init__(self, parent, logpan=None):
        """
        Initialise l'objet.

        params:
          - parent: parent du gui.
          - logpan: [LogDisplayPanel] le panneau où afficher les logs.
        """
        super().__init__(parent)

        self.liste_trb = []
        self.pdf_rep = None

        self.liste = tk.Listbox(self, selectmode=tk.MULTIPLE, width=40, height=40)
        self.liste.bind("<<ListboxSelect>>", self.clic_liste)
        self.scroll = tk.Scrollbar(self, command=self.liste.yview)
        self.liste["yscrollcommand"] = self.scroll.set
        pan = ttk.Frame(self)
        self.butsel = tk.Button(pan, text="Tout sélectionner", command=self.clic_select_all)
        self.butunsel = tk.Button(pan, text="Annuler la sélection", command=self.clic_unselect_all)

        self.butsel.pack(side=tk.LEFT)
        self.butunsel.pack(side=tk.RIGHT)

        pan.pack(side=tk.TOP)
        self.liste.pack(side=tk.LEFT, expand=False, fill=tk.NONE)
        self.scroll.pack(side=tk.RIGHT, expand=True, fill=tk.Y)

        self.logpan = None
        self.set_log_pan(logpan)



    def set_log_pan(self, logpan):
        """
        garde en mémoire le panneau d'affichage des logs.

        params:
          - logpan: [LogDisplayPanel] panneau d'affichage des logs.

        return: None
        """
        if self.logpan != logpan:
            self.logpan = logpan
            if self.logpan != None:
                self.logpan.set_selection_pan(self)
                self.clic_liste()



    def set_pdf_directory(self, rep):
        """
        Fixe le dossier où seront stockés les fichiers pdf générés
        """
        self.pdf_rep = rep



    def populate_liste(self, tab):
        """
        met à jour la liste des fichiers trb à disposition.

        params:
          - tab: [list] de string.

        return: None
        """
        self.liste.delete(0, self.liste.size())
        self.liste_trb = []
        for elmnt in tab:
            self.liste_trb.append(elmnt)
            self.liste.insert(tk.END, os.path.basename(elmnt))



    def clic_select_all(self):
        """
        gestion du bouton select-all. Sélectionne toute la liste.

        return: None
        """
        self.liste.selection_set(0, tk.END)
        self.clic_liste()



    def clic_unselect_all(self):
        """
        gestion du bouton unselect-all. déselectionne toute la liste.

        return: None
        """
        self.liste.selection_clear(0, tk.END)
        self.clic_liste()



    def clic_liste(self, event=None):
        """
        Informe le [LogDisplayPanel] si on a au moins un fichier sélectionné.
        Ceci lui permet de mettre à jour l'état de ses widgets (disabled/normal)

        params:
          - event: evenement envoyé par la listbox

        return: None
        """
        disable = len(self.liste.curselection()) == 0
        if self.logpan != None:
            self.logpan.disable_me(disable)



    def get_selection(self):
        """
        Permet d'obtenir la liste des fichiers trb sélectionnés.

        return: [list] de string
        """
        return [self.liste_trb[i] for i in self.liste.curselection()]



    def get_pdf_directory(self):
        """
        return [string] le dossier dans lequel il faudra enregistrer les fichier pdf générés
        """
        return self.pdf_rep



    def disable_me(self, disable=True):
        """
        Met à jour les boutons et la liste.
        Permet de désactivé s'il n'y a pas lieu de devoir cliquer dessus.

        params:
          - disable: [booléen] Si True, on désactive tout le monde, sinon, active tout le monde.

        return: None
        """
        state = tk.DISABLED if disable else tk.NORMAL
        self.liste["state"] = state
        self.butsel["state"] = state
        self.butunsel["state"] = state









class GenerationManager(TaskManagerThread):
    """
    Task Manager qui crée les fichiers pdf.
    """

    def __init__(self, log_panel, controle_panel):
        """
        Initialise l'objet

        params:
          - log_panel: [TaskManagerPanel] qui permet de logger les sorties
          - controle_panel: [LogDisplayPanel] qui contrôle l'execution des taches.
        """
        super().__init__(log_panel)
        self.controle_panel = controle_panel



    def nothing_to_do(self):
        """
        Que faire s'il n'y a plus rien à faire ?

        return: None
        """
        self.controle_panel.clic_stop()
        self.controle_panel.selpan.clic_unselect_all()



    def manage_the_task(self, data):
        """
        Effectue la tache à accomplir, c'est à dire la génération du pdf à partir du trb.
        ATTENTION: aucune vérification n'est effectué pour savoir si un fichier pdf existe déjà.
        Il se peut donc qu'on écrase des fichiers.

        params:
          - data: [list] couple (string, string). La première est le nom du fichier trb,
                  la deuxième est le nom du futur fichier pdf.

        return: True si tut s'est bien passé, False si une erreur est survenue ou si on a stoppé la tache.
        """
        trb_file, pdf_file = data
        self.log_message("Génération de " + os.path.basename(pdf_file), cli_log=True)

        # lecture du fichier trb
        try:
            trb = TrbClasse(ficname=trb_file)
        except:
            return False

        doc = PdfConverter(trb, lambda x,y:print(x, y)).get_pdf_document(show_boxes=False)
        doc.save(pdf_file)
        return True









class LogDisplayPanel(ttk.Frame):
    """
    Panneau contenant les logs de création de pdf ainsi que les boutons de démarrage et stop des taches.
    """

    def __init__(self, parent, select_pan=None):
        """
        Initialise l'objet

        params:
          - parent: parent du gui
          - select_pan: [GenerateSelectionPanel] panneau de sélectoin des fichiers.
        """
        super().__init__(parent)

        self.butgen = tk.Button(self, text="Générer", command=self.clic_generate)
        self.butstp = tk.Button(self, text="Arrêt", command=self.clic_stop)
        self.butstp["state"] = tk.DISABLED
        self.log_panel = TaskManagerPanel(self)
        self.generation = GenerationManager(self.log_panel, self)
        self.log_panel.set_manager(self.generation)
        self.generation.start_manager()

        self.log_panel.pack(side=tk.BOTTOM)
        self.butgen.pack(side=tk.LEFT, padx=100)
        self.butstp.pack(side=tk.RIGHT, padx=100)

        self.selpan = None
        self.set_selection_pan(select_pan)



    def set_selection_pan(self, pan):
        """
        Garde en mémoire le panneau de sélection si ça n'a pas été fait lors de l'initialisation.

        params:
          - pan: [GenerateSelectionPanel] Paneau de sélection des fichiers.

        return: None
        """
        if self.selpan != pan:
            self.selpan = pan
            if self.selpan != None:
                self.selpan.set_log_pan(self)



    def clic_generate(self):
        """
        Gestion du clic pour commencer la génération de pdf:
        Envoi des taches dans la file, mise à jour de l'état des boutons.

        return: None
        """
        if self.selpan != None:
            selects = self.selpan.get_selection()
            pdf_rep = self.selpan.get_pdf_directory()
            self.selpan.disable_me()
            for sel in selects:
                psel = sel if pdf_rep == None else os.path.join(pdf_rep, os.path.basename(sel))
                psel = psel[:-3] + "pdf"
                self.log_panel.add_task([sel, psel])
        self.butgen["state"] = tk.DISABLED
        self.butstp["state"] = tk.NORMAL



    def clic_stop(self):
        """
        Gestion du clic pour arreter la génération:
        envoi du signal de STOP au manager et mise à jour de l'état des boutons.

        return: None
        """
        if self.selpan != None:
            self.selpan.disable_me(False)
        self.generation.ask_for_stop()
        self.butgen["state"] = tk.NORMAL
        self.butstp["state"] = tk.DISABLED



    def stop_manager(self):
        """
        Permet de stopper de manière permanente le gestionnaire des taches.
        Utile pour quitter proprement.

        return: None
        """
        self.log_panel.stop_manager()



    def disable_me(self, disable):
        """
        Permet de disabled le bouton de génération.
        Utile quand aucun fichier n'est sélectionné.

        params:
          - disable: [booléen] Si True, on disable, sinon on rend met l'état normal.

        return: None
        """
        state = tk.DISABLED if disable else tk.NORMAL
        self.butgen["state"] = state














if __name__ == "__main__":
    wind = tk.Tk()
    wind.title("Génération des trombis")

    panneau = GenerationPanel(wind)
    panneau.pack()

    fichiers = []
    if len(sys.argv) == 1:
        # rien
        fichiers = [os.path.abspath(fic) for fic in os.listdir(os.getcwd())]
    elif len(sys.argv) == 2 and os.path.isdir(sys.argv[1]):
        # un seul repertoire
        fichiers = [os.path.abspath(os.path.join(sys.argv[1], fic)) for fic in os.listdir(sys.argv[1])]
    else:
        # un ou des fichiers
        fichiers = [os.path.abspath(fic) for fic in sys.argv[1:]]

    listing = []
    for fic in fichiers:
        if fic.endswith(".trb"):
            if os.path.isfile(fic):
                listing.append(fic)

    panneau.populate_files(listing)


    wind.mainloop()


#
#
# END
#
#
