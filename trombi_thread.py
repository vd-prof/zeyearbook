import threading
import time

import tkinter as tk
from tkinter import ttk




class TaskManagerThread(threading.Thread):
    """
    Classe gérant un processus qui execute des taches en parallèle de tkinter.
    Elle gère aussi une file d'attente des taches à accomplir.
    Elle est accompagnée d'un TaskManagerPanel qui permet d'afficher l'évolution des taches.

    Le processus est lancé par `TaskManagerThread.start_manager()`.
    Celui-ci attend des taches à executer.
    Il est arrété automatiquement si le TaskManagerPanel est détruit (fenêtre quittée)
    ou en utilisant TaskManagerThread.ask_for_quit()

    Pour implementer cette classe, on doit surcharger les méthodes `manage_the_task(data)` et `nothing_to_do`.
    La première effectue réellement la tache.
    La deuxième est appelée une fois que la file des taches est vide.

    On peut demander au processus d'arreter la tache en cours avec TaskManagerThread.ask_for_stop()
    Pour cette denière fonctionnalité, il faut que pendant la tache, on vérifie si TaskManagerThread.is_stopped == True
    et dans ce cas arreter le traitement.
    Il n'est pas possible, à ma connaissance, de stopper le traitement de manière générique sans intervention dans la classe fille.
    """

    """ Le processus n'est pas encore actif ou est arrété """
    UNAVAILABLE = -1

    """ Le processus est au travail ou attend du travail """
    NORMAL = 0

    """ Le travail actuel doit être stoppé mais pas le processus """
    STOPPED = 1

    """ Le travail actuel doit être stoppé et le processus doit être arrété """
    QUITTED = 2

    def __init__(self, log_panel):
        """
        Initialise l'objet

        params:
          - log_panel: [TaskManagerPanel] pour afficher la progression des taches
        """
        super().__init__()
        self.log_panel = log_panel
        self.tasks = []
        self.lock = threading.Lock()
        self.set_thread_mode(TaskManagerThread.UNAVAILABLE)



    def start_manager(self):
        """
        Démarre le processus. Celui-ci attend les taches à accomplir.

        return: None
        """
        self.set_thread_mode(TaskManagerThread.NORMAL)
        self.start()



    def stack_task(self, data):
        """
        Ajoute une tache à effectuer à la file des travaux.

        params:
          - data: une tache. Ce peut être n'importe quoi, tant que c'est compris par `self.manage_the_task()`.

        return: None
        """
        with self.lock:
            self.tasks.append(data)



    def log_message(self, message, gui_log=True, cli_log=False):
        """
        Demande l'affichage d'un message par un TaskManagerPanel

        params:
          - message: [string] message à afficher
          - gui_log: [booléen] doit-on afficher le message sur le TaskManagerPanel ?
          - cli_log: [booléen] doit-on afficher le message sur la console ?

        return: None
        """
        if gui_log:
            self.log_panel.add_text(message)
        if cli_log:
            print(message)



    def set_thread_mode(self, mode):
        """
        Change le mode du thread. Il peut être UNAVAILABLE, NORMAL, STOPPED ou QUITTED

        return None
        """
        with self.lock:
            self.state = mode



    def ask_for_quit(self):
        """
        Demande au processus de s'arréter.
        Ceci stoppe aussi la tache en cours (si implémenté par la classe fille).

        return: None
        """
        self.set_thread_mode(TaskManagerThread.QUITTED)



    def ask_for_stop(self):
        """
        Demande au processus d'arreter la tache en cours (si implémenté par la classe fille).
        Ne stoppe pas le processus.

        return: None
        """
        self.set_thread_mode(TaskManagerThread.STOPPED)



    @property
    def is_stopped(self):
        """
        Le processus est-il en mode stop ?
        Si oui, il doit arreter la tache actuelle mais continuer à tourner.

        return [booléen]
        """
        stat = TaskManagerThread.UNAVAILABLE
        with self.lock:
            stat = self.state
        return stat >= TaskManagerThread.STOPPED



    @property
    def is_quitted(self):
        """
        Le processus est-il en mode quit ?
        Si oui, il doit arreter la tache actuelle est s'arreter.

        return [booléen]
        """
        stat = TaskManagerThread.UNAVAILABLE
        with self.lock:
            stat = self.state
        return stat == TaskManagerThread.QUITTED



    @property
    def is_running(self):
        """
        Le procesus est-il en train de tourner ?
        Dans ce mode, soit il attend un tache, soit il est en train d'en effectuer une.

        return [booléen]
        """
        stat = TaskManagerThread.UNAVAILABLE
        with self.lock:
            stat = self.state
        return stat == TaskManagerThread.NORMAL



    def run(self):
        """
        Methode principale. C'est elle qui gère la file d'attente et lance les taches une par une.

        return: None
        """
        everything_done = True
        while self.is_running:
            with self.lock:
                current = self.tasks.pop(0) if len(self.tasks) > 0 else None
            if current == None:
                if not everything_done:
                    everything_done = True
                    self.nothing_to_do()
                time.sleep(1)
            else:
                everything_done = False
                self.log_panel.add_text("DEBUT", prefix="---------- ", suffix=" ----------", is_important=True)
                res = self.manage_the_task(current)
                if res and self.is_running:
                    self.log_panel.add_text("FIN", prefix="----------- ", suffix=" -----------", is_important=True)
                else:
                    self.log_panel.add_text("ARRET", prefix="---------- ", suffix=" ----------", is_important=True)
            if self.is_stopped and not self.is_quitted:
                self.set_thread_mode(TaskManagerThread.NORMAL)



    def manage_the_task(self, data):
        """
        Méthode abstraite qui traite une tache.
        Cette méthode doit regarder régulièrement is_stopped ou is_quitted pour s'arreter au besoin.

        params:
          - data: la tache à accomplir.

        return: [booléen] True si la tache s'est a été traitée correctement.
                False s'il y a eu un problème durant lr traitement de la tache.
        """
        return True



    def nothing_to_do(self):
        """
        Méthode abstraite qui est appelée quand la file devient vide.
        """
        pass









class TaskManagerPanel(ttk.Frame):
    """
    ttk.Frame contenant un tk.Text.
    Celui-ci est destiné à accueillir les logs du traitement des taches fait par un TaskManagerThread.
    Une Scrollbar est présente pour naviguer dans les logs.
    On a aussi un label qui affiche des messages importants (typiquement des étapes du traitement)
    """

    def __init__(self, parent):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
        """
        super().__init__(parent)

        self.label = tk.Label(self, text="Rien pour l'instant")
        self.label.pack(side=tk.BOTTOM, expand=False, fill=tk.Y)

        self.textbox = tk.Text(self, width=80, height=40)
        self.scroll = tk.Scrollbar(self, command=self.textbox.yview)
        self.textbox["yscrollcommand"] = self.scroll.set
        self.scroll.pack(side=tk.RIGHT, fill=tk.Y)
        self.textbox.pack(side=tk.LEFT, expand=True, fill=tk.BOTH)

        self.task_manager = None



    def set_manager(self, task_manager):
        """
        Permet de mettre en place le task_manager.

        params:
          - task_manager: [TaskManagerThread] le task manager
        """
        self.task_manager = task_manager



    def add_text(self, texte, prefix="", suffix="", newline="\n", is_important=False):
        """
        Log un message.
        Affiche prefix + message + suffixe + newline
        Si le message est important, il est aussi affiché dans le Label.

        Si un problème survient dans l'affichage, on demande au TaskManagerThread de s'arreter.

        params:
          - texte: [string] message à publier
          - prefix: [string] un prefix au message
          - suffix: [string] un suffixe au message
          - newline: [string] la string qui sert de fin de ligne
          - is_important: [booléen] Si True, le message est affiché aussi sur le Label

        return: None
        """
        try:
            pos = self.scroll.get()
            self.textbox.insert(tk.END, prefix + texte + suffix + newline)
            if is_important:
                self.label["text"] = texte
            if pos[1] > 0.99:
                self.textbox.yview(tk.END)
        except:
            self.stop_manager()



    def stop_job(self):
        """
        Demande au TaskManagerThread d'arreter la tache en cours mais pas le processus.

        return: None
        """
        self.task_manager.ask_for_stop()



    def stop_manager(self):
        """
        Demande au TaskManagerThread d'arreter la tache en cours et le processus de gestion de tache.

        return: None
        """
        self.task_manager.ask_for_quit()



    def add_task(self, data):
        """
        Ajoute une tache au gestionnaire de tache.
        Elle sera inclue dans sa file et sera traitée en temps voulu

        params:
          - data: la tache à traiter

        return: None
        """
        self.task_manager.stack_task(data)



    def destroy(self):
        """
        Si ce TaskManagerPanel se fait détruire (typiquement, on demande la fermeture de la fenête),
        on force le TaskManagerThread de d'arrêter aussi.
        """
        super().destroy()
        self.stop_manager()




#
#
# END
#
#
