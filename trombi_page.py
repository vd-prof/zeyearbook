import os.path

from PIL import Image, ImageTk

import tkinter as tk
from tkinter import ttk

from tkinter import filedialog as fd
from tkinter.messagebox import showwarning

from trombi_gui_lib import *
from trombi_trb import *
from trombi_converter import *
from trombi_eleve import PanneauEleve




class PanneauPage(ttk.Frame):
    """
    Panneau contenant tout ce qu'il faut pour modifier les paramètres de la page:
     Les paramètres et la visualisation de la page.
    """

    def __init__(self, parent, real_data=True):
        """
        Initialise l'objet

        params:
          - parent: parent du gui
          - real_data: [boolean] Si False, on empèche l'édition du nom de la classe et on adapte le nombre d'élèves pour toujours remplir la page
        """
        super().__init__(parent)

        self.para_pan = PanneauParametresPage(self, real_data)
        self.canv_pan = PanneauCanvas(self)

        self.para_pan.grid(row=0, column=0, sticky=tk.N, padx=20, pady=(10,0))
        self.canv_pan.grid(row=0, column=1, sticky=tk.N, pady=(10,0))

        self.eleve_panel = None
        self.info_panel = None

        if real_data:
            self.eleve_panel = PanneauEleve(self)
            self.eleve_panel.grid(row=0, column=2, sticky=tk.N, padx=20, pady=(10,0))



    def set_info_panel(self, info_panel):
        """
        Fixe le panneau d'infos

        params:
          - info_panel: [ModificationPanel]
        """
        self.info_panel = info_panel



    def show_boxes_version(self):
        """
        Quelle version doit être afficher ? Version finale ou avec les boites ?

        return True si avec les boites, False si version finale
        """
        if self.info_panel == None:
            return True
        return self.info_panel.show_boxes_version()



    def show_numbers(self, show):
        """
        Transmet à la visualisation l'ordre d'afficher/effacer les numéros des boites

        params:
          - show: [booleén] True pour afficher les numéros.
        """
        self.canv_pan.show_numbers(show)



    def redrawn_needed(self):
        """
        Transmet à la visualisation l'ordre de rafraichir l'image.
        """
        self.canv_pan.redraw_me()



    def load_trb(self, trb_data):
        """
        Charge un nouveau trb dans les différents panneaux.

        params:
          - trb_data: [TrbClasse]
        """
        self.para_pan.load_trb(trb_data)
        self.canv_pan.load_trb(trb_data)
        if self.eleve_panel != None:
            self.eleve_panel.load_trb(trb_data)



    def reload_trb(self):
        """
        Recharge les données. Permet d'actualiser certains affichages.
        """
        self.load_trb(self.canv_pan.trb)
        if self.info_panel != None:
            self.info_panel.reload_trb()



    def save_trb(self):
        """
        Sauve les données trb.
        """
        self.para_pan.save_trb()
        if self.eleve_panel != None:
            self.eleve_panel.save_trb()



    def select_eleve(self, eleve):
        """
        Transmet la selection d'un élève.

        params:
          - eleve: [TrbEleve]
        """
        self.canv_pan.select_eleve(eleve)
        self.maj_eleve(eleve)



    def maj_eleve(self, eleve):
        """
        Met à jour les information d'un élève.

        params:
          - eleve: [trbEleve]
        """
        if self.eleve_panel != None:
            self.eleve_panel.load_eleve(eleve)









class PanneauNomClasse(AbstractLabelFrame):
    """
    Sous-panneau contenant les options concernant le nom de la classe.

      - changer le nom de la classe
      - afficher ou pas le nom de la classe
      - changer la hauteur réservée au titre (en cas d'affichage)
      - changer la taille de la font
      - changer la font (par son nom)
    """

    def __init__(self, parent, page_panel, change_nom_classe=True):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
          - page_panel: [PanneauPage] panneau représentant la page
          - allow_nom_classe: [booléen] autorise-t-on à changer le nom de la classe ?
        """
        super().__init__(parent, "Nom de la classe", page_panel)
        self.change_nom_classe = change_nom_classe

        self.font_panel = FontPanel(self, font_name_key="nom_font_classe", font_size_key="taille_font_classe", height_key="hauteur_nom_classe")
        self.font_panel.pack(side=tk.BOTTOM)

        self.value_nom = tk.StringVar()
        self.old_value_nom = ""
        self.entry_nom = ttk.Entry(self, width=9, textvariable=self.value_nom)
        if change_nom_classe:
            self.entry_nom.pack(side=tk.TOP, padx=4, pady=4)
            self.entry_nom.bind('<Return>', self.valide_entry)
            self.entry_nom.bind('<KP_Enter>', self.valide_entry)
            self.entry_nom.bind('<FocusOut>', self.valide_entry)
        else:
            self.value_nom.set("Nom de classe")
            self.entry_nom['state'] = tk.DISABLED

        self.ischecked = tk.BooleanVar()
        self.ischecked.set(True)
        self.old_status = self.ischecked.get()
        tk.Checkbutton(self, text="Afficher le nom", variable=self.ischecked,
                       onvalue=True, offvalue=False, command=self.change_checkbutton).pack(side=tk.TOP)

        self.change_checkbutton(action=False)
        self.trb = None



    def save_trb(self):
        """
        Sauve les modifications éventielles du trb.

        return: True si une modification a été faite
        """
        res = False
        if self.trb != None:
            if self.trb.nom_classe != self.old_value_nom:
                self.trb.nom_classe = self.old_value_nom
                res = True
            if self.trb.affiche_nom_classe != self.ischecked.get():
                self.trb.affiche_nom_classe = self.ischecked.get()
                res = True
            if self.font_panel.save_trb():
                res = True
        return res



    def load_trb(self, trb):
        """
        Charge un trb.
        Met à jour les différents GUI en conséquence.

        params:
          - trb: [TrbClasse] données à traiter

        return: None
        """
        self.trb = trb
        if not self.change_nom_classe:
            self.trb.nom_classe = "Nom de la Classe"
        self.old_value_nom = self.trb.nom_classe
        self.value_nom.set(self.old_value_nom)
        self.old_status = self.trb.affiche_nom_classe
        self.ischecked.set(self.old_status)
        self.change_checkbutton(action=False)
        self.font_panel.load_trb(trb)



    def change_checkbutton(self, action=True):
        """
        Gestion du clic de la checkbox d'affichage de nom

        params:
          - action: [booléen] si action  == True, on demande le redraw de la page.

        return: None
        """
        self.font_panel.set_state( tk.NORMAL if self.ischecked.get() else tk.DISABLED )
        if self.ischecked.get() != self.old_status:
            self.old_status = self.ischecked.get()
            if action:
                self.action_needed()



    def valide_entry(self, event):
        """
        Gestion de la modification de l'entry nom de classe

        params:
          - event: l'event envoyé par le bind des widgets

        return: None
        """
        val = self.value_nom.get()
        if self.old_value_nom != val:
            self.old_value_nom = val
            self.save_trb()
            self.page_panel.reload_trb()









class PanneauImageVide(AbstractLabelFrame):
    """
    Sous-panneau gérant les options de chaise vide.

      - doit-on afficher une image ?
      - fichier image par défaut
    """

    def __init__(self, parent, page_panel):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
          - page_panel: [PanneauPage] panneau repésentant la page.
        """
        super().__init__(parent, "Chaise vide", page_panel)

        self.ischecked = tk.BooleanVar()
        self.old_status = self.ischecked.get()
        tk.Checkbutton(self, text="Image par défaut", variable=self.ischecked,
                       onvalue=True, offvalue=False, command=self.change_checkbutton).pack(side=tk.TOP)

        self.label = ttk.Label(self, text="Fichier image")
        self.label.pack(side=tk.LEFT, padx=4)

        self.old_value = ""
        self.button = ttk.Button(self, text="---", command=self.select_image)
        self.button.pack(side=tk.RIGHT, padx=4, pady=4)

        self.change_checkbutton(action=False)
        self.trb = None



    def save_trb(self):
        """
        Sauve les données dans le trb.

        return: True si quelque chose a été modifié
        """
        change = False
        if self.trb != None:
            if self.trb.image_chaise_vide != self.ischecked.get():
                self.trb.image_chaise_vide = self.ischecked.get()
                change = True
            val = self.old_value if self.old_value != "---" else ""
            if self.trb.fichier_chaise_vide != val:
                self.trb.fichier_chaise_vide = val
                change= True
        return change



    def load_trb(self, trb):
        """
        Charge les données d'un trb.
        Met à jour les éléments gui.

        params:
          - trb: [TrbClasse] données trb

        return: None
        """
        self.trb = trb
        self.ischecked.set(self.trb.image_chaise_vide)
        self.set_button_text(self.trb.fichier_chaise_vide)
        self.change_checkbutton(action=False)



    def change_checkbutton(self, action=True):
        """
        Gestion du clic sur la checkbox.

        return: None
        """
        state = tk.NORMAL if self.ischecked.get() else tk.DISABLED
        self.label['state'] = state
        self.button['state'] = state
        if self.ischecked.get() != self.old_status:
            self.old_status = not self.ischecked.get()
            if action:
                self.action_needed()



    def set_button_text(self, chaine):
        """
        Mise à jour du nom du fichier dans le bouton.
        Si le nom est trop long, on coupe.
        S'il est vide, on remplace par "---"

        params:
          - chaine: [string] nom du fichier

        return: None
        """
        self.old_value = "---" if chaine == None or len(chaine) == 0 else chaine
        base = os.path.basename(self.old_value) if self.old_value != '---' else '---'
        if len(base) > 10:
            base = base[0:7] + "..."
        self.button['text'] = base



    def select_image(self):
        """
        Gestion du clic sur le bouton de choix du fichier image par défaut.

        return: None
        """
        name = fd.askopenfilename(initialdir=".", title="Image par défaut", filetypes=(("fichier PNG", "*.png"),("fichier JPEG", "*.jpg")))
        if len(name) > 0 and self.old_value != name:
            self.set_button_text(name)
            self.action_needed()









class PanneauHauteurNom(AbstractLabelFrame):
    """
    Sous-panneau de gestion des options générales des noms des élèves

      - hauteur réservée au nom des élèves
      - changer la taille de la font
      - changer la font (par son nom)
    """

    def __init__(self, parent, page_panel):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
          - page_panel: [PanneauPage] panneau représentant la page
        """
        super().__init__(parent, "Élèves", page_panel)

        self.panel = FontPanel(self, font_name_key="nom_font_eleve", font_size_key="taille_font_eleve", height_key="hauteur_nom_eleve")
        self.panel.pack()



    def save_trb(self):
        """
        Sauve les données dans le trb.

        return: True si quelque chose a été modifié
        """
        return self.panel.save_trb()



    def load_trb(self, trb):
        """
        Charge les données d'un trb.
        Met à jour les éléments GUI

        params:
          - trb: [TrbClasse] données trb

        return: None
        """
        self.panel.load_trb(trb)









class PanneauTaillePage(GenericTwoValuesPanel):
    """
    Sous-panneau de gestion des options de la taille de la page

      - hauteur de la page
      - largeur de la page
    """

    def __init__(self, parent, page_panel):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
          - page_panel: [PanneauPage] panneau représentant la page.
        """
        super().__init__(parent,
                         page_panel=page_panel,
                         label_text="Taille de la page",
                         label_entry1="largeur:",
                         label_entry2="hauteur:",
                         trb_field1="largeur_page",
                         trb_field2="hauteur_page",
                         type_field=float,
                         nb_char_displayed=5)









class PanneauMargePage(GenericTwoValuesPanel):
    """
    Sous-panneau de gestion des options des marges externieures de la page

      - marges hozizontales
      - marges verticales
    """

    def __init__(self, parent, page_panel):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
          - page_panel: [PanneauPage] panneau représentant la page.
        """
        super().__init__(parent,
                         page_panel=page_panel,
                         label_text="Marges de la page",
                         label_entry1="horizontales:",
                         label_entry2="verticales:",
                         trb_field1="marge_horizontale_page",
                         trb_field2="marge_verticale_page",
                         type_field=float,
                         nb_char_displayed=4)









class PanneauMargeEleve(GenericTwoValuesPanel):
    """
    Sous-panneau de gestion des distances inter-elèves

      - marges horizontales
      - marges verticales
    """

    def __init__(self, parent, page_panel):
        """
        Initialise l'objet

        params:
          - parent: parent du GUI
          - page_panel: [PanneauPage] panneau représentant la page.
        """
        super().__init__(parent,
                         page_panel=page_panel,
                         label_text="Marges entre élèves",
                         label_entry1="horizontales:",
                         label_entry2="verticales:",
                         trb_field1="marge_horizontale_eleves",
                         trb_field2="marge_verticale_eleves",
                         type_field=float,
                         nb_char_displayed=4)









class PanneauDisposition(GenericTwoValuesPanel):
    """
    Sous-panneau de gestion des options de dispositions des élèves.

      - nombre de lignes (0: calcul optimal)
      - nombre de colonnes (0: calcul optimal)
    """

    def __init__(self, parent, page_panel, adapte_eleves):
        """
        Initialise l'objet.
        On peut adapter le nombre d'élèves à la disposition choisie. Ceci permet de visualiser rapidement à quoi
        va ressembler la page. Les élèves ainsi adaptés n'auront pas de nom ni image.

        params:
          - parent: parent du GUI
          - page_panel: [PanneauPage] panneau représentant la page.
          - adapte_eleves: [booléen] adapte-t-on le nombre d'élèves à la disposition choisie ?
        """
        super().__init__(parent,
                         page_panel=page_panel,
                         label_text="Disposition",
                         label_entry1="nb de lignes:",
                         label_entry2="nb de colonnes:",
                         trb_field1="nb_lignes",
                         trb_field2="nb_colonnes",
                         type_field=int,
                         nb_char_displayed=3,
                         unit_display=False)
        self.adapte = adapte_eleves



    def load_trb(self, trb):
        """
        Surcharge l'initialisation des données pour adapter
        le nombre d'élèves si besoin
        """
        super().load_trb(trb)
        if self.adapte:
            self.save_trb()



    def save_trb(self):
        """
        Sauve les données dans le trb.

        return: None
        """
        res = super().save_trb()
        if self.adapte:
            nblig = self.old_val1 if self.old_val1 != 0 else 5
            nbcol = self.old_val2 if self.old_val2 != 0 else 7

            if self.trb != None:
                self.trb.eleves = [TrbEleve(classe=self.trb) for _ in range(nblig*nbcol)]
                self.trb.eleves[0].nom = ["NOM", "Prénom"]
                self.trb.eleves[1].nom = ["NOM -", "COMPLET", "Prénom"]
        return res









class PanneauParametresPage(tk.LabelFrame):
    """
    Panneau latéral général de l'interface

    Contient les différents panneaux contenant les options générales:
      - nom de la classe,
      - disposition des élèves
      - taille de la page
      - marges externes et internes
      - image par défaut de la chaise vide
    """

    def __init__(self, parent, real_data=True):
        """
        Initialise l'objet

        params:
          - parent: [PanneauPage] parent du GUI
          - change_nom_classe: [booléen] autorise-t-on à changer le nom de la classe ?
          - adapte_eleves: [booléen] adapte-t-on le nombre d'élèves à la disposition choisie ?
        """
        super().__init__(parent, text="Options de la page", font=('Helvetica 16 bold'), labelanchor=tk.N)

        self.pan_classe = PanneauNomClasse(self, parent, change_nom_classe=real_data)
        self.pan_dispo = PanneauDisposition(self, parent, adapte_eleves=not real_data)
        self.pan_image_vide = PanneauImageVide(self, parent)
        self.pan_hauteur_nom = PanneauHauteurNom(self, parent)
        self.pan_marge_eleve = PanneauMargeEleve(self, parent)
        self.pan_marge_page = PanneauMargePage(self, parent)
        self.pan_taille_page = PanneauTaillePage(self, parent)

        self.pan_classe.pack(     side=tk.TOP, fill=tk.X, expand=True, padx=20, pady=10)
        self.pan_dispo.pack(      side=tk.TOP, fill=tk.X, expand=True, padx=20, pady=10)
        self.pan_image_vide.pack( side=tk.TOP, fill=tk.X, expand=True, padx=20, pady=10)
        self.pan_hauteur_nom.pack(side=tk.TOP, fill=tk.X, expand=True, padx=20, pady=10)
        self.pan_marge_eleve.pack(side=tk.TOP, fill=tk.X, expand=True, padx=20, pady=10)
        self.pan_marge_page.pack( side=tk.TOP, fill=tk.X, expand=True, padx=20, pady=10)
        self.pan_taille_page.pack(side=tk.TOP, fill=tk.X, expand=True, padx=20, pady=10)

        self.help_but = ttk.Button(self, text="Aide", command=self.show_help)
        self.help_but.pack(pady=(0,10))

        self.panels = (self.pan_classe, self.pan_dispo, self.pan_image_vide, self.pan_hauteur_nom,
                       self.pan_marge_eleve, self.pan_marge_page, self.pan_taille_page)



    def show_help(self):
        """
        Affiche une aide.
        """
        ModalHelp.display_help(self)



    def load_trb(self, trb_data):
        """
        Charge un trb.
        Se contente de déléguer aux autres panneaux

        params:
          - trb_data: [TrbClasse] données trb

        return: None
        """
        for pan in self.panels:
            pan.load_trb(trb_data)



    def save_trb(self):
        """
        Sauve un trb.
        Se contente de mettre à jour le trb. Pas d'accès disque.

        return: None
        """
        res = False
        for pan in self.panels:
            if pan.save_trb():
                res = True
        return res









class PanneauCanvas(tk.Canvas):
    """
    Panneau représentant la page.

    C'est un canvas sur lequel on affiche une image de la page.
    Elle contient les marges, les contours des boites élèves et les photos.

    le canvas réagit au clic pour sélection de la boite élève.
    """

    def __init__(self, parent):
        """
        Initialise l'objet

        params:
          - parent: [PanneauPage] parent du GUI
        """
        super().__init__(parent, height=594, width=420)
        self.page_panel = parent
        self.trb = None
        self.image_page = None  # image de la page
        self.boxes = []  # boites élève sur la page
        self.selected_elements = None  # couple (image_alpha, boite) sélectionné
        self.alphas = []  # liste des image_alpha de mise en surbrillance des boites
        self.bind('<Button-1>', self.click_box)

        self.ALPHA = (0, 0, 0, 100)  # couleur RGBA d'une boite sélectionnée



    def load_trb(self, trb_data):
        """
        Charge les données d'un trb classe et met à jour le GUI.

        params:
          - trb_data: [TrbClasse] données trb d'une classe

        return: None
        """
        self.trb = trb_data
        self.select_box(None)
        self.redraw_me(keep_selected=False)



    def select_eleve(self, eleve):
        """
        Selectionne un élève.
        Affiche la boite en surbrillance et les données dans le panneau élève

        params:
          - eleve: [TrbEleve] l'élève à selectionner ou None si on déselectionne

        return: None
        """
        boite = None
        for box in self.boxes:
            if box.eleve == eleve:
                boite = box
                break
        self.select_box(boite)



    def click_box(self, event):
        """
        Sélectionne une boite-élève
        Affiche la boite en surbrillance et les données dans le panneau élève

        params:
          - event: l'event à l'origine d'un clic de souris

        return: None
        """
        x, y = event.x, event.y
        boite = None
        for box in self.boxes:
            if box.contains_point(x, y):
                boite = box
                break
        self.select_box(boite)



    def select_box(self, boite):
        """
        Met en surbrillance la boite correspondante et
        demande la mise à jour des données dans le panneau élève.

        params:
          - boite: [GuiEleveBox] la boite à mettre en valeur ou None pour déselectionner.
        """
        if self.selected_elements != None and self.selected_elements[1] == boite:
            return
        self.selected_elements = None
        if boite != None:
            image = self.draw_alpha_boite(boite)
            self.selected_elements = (image, boite)
        self.page_panel.maj_eleve(boite.eleve if boite != None else None)



    def draw_alpha_boite(self, boite, msg=None):
        """
        Affiche la boite en surbrillance ainsi qu'un message dessus si besoin

        params:
          - boite: [GuiEleveBox]
          - msg: [string | None]
        """
        w = int(boite.xmax-boite.xmin) + 2
        h = int(boite.ymax-boite.ymin) + 2
        image = Image.new(mode='RGBA', size=(w, h), color=self.ALPHA)
        image = ImageTk.PhotoImage(image)
        self.create_image(boite.xmin-1, boite.ymin-1, image=image, anchor=tk.NW)
        if msg != None:
            self.create_text(boite.xmin + w/2, boite.ymin + h/2, text=msg, fill="white", tag="numbers")
        return image



    def show_numbers(self, show=True):
        """
        Si show == True: Met toutes les boites en surbrillance et affiche les numéros des boites
        Sinon enlève les numéros et la surbrillance

        params:
          - show: [booléen] affiche/efface le numéro des boites

        return: None
        """
        self.alphas = []
        self.selected_elements = None
        self.delete("numbers")
        if show:
            for k, boite in enumerate(self.boxes):
                image = self.draw_alpha_boite(boite, str(k+1))
                self.alphas.append(image)



    def redraw_me(self, keep_selected=True):
        """
        Redessine la représentation de la page.

        return: None
        """
        boite = None
        if self.selected_elements != None and keep_selected:
            boite = self.selected_elements[1]
        self.show_numbers(show=False)

        if self.trb != None:
            conv = PixmapConverter(self.trb, lambda x,y:showwarning(x, y))
            self.image_page = conv.get_tkImage(show_boxes=self.page_panel.show_boxes_version())
            self.config(width=conv.width, height=conv.height)
            self.create_image(0, 0, image=self.image_page, anchor=tk.NW)
            self.boxes = conv.get_boxes()
        if boite != None and boite.eleve in self.trb.eleves:
            self.select_eleve(boite.eleve)
        else:
            self.select_box(None)







class ModalHelp(tk.Toplevel):
    """
    Fenetre montrant les paramètres généraux de la page.
    Ceci pour configurer les fichiers par lot.
    """

    def __init__(self, parent):
        """
        Initialise l'objet

        params:
          - parent: fenetre parente du GUI.
        """
        super().__init__(parent)
        tex = tk.Text(self, width=65, height=25)
        scro = tk.Scrollbar(self, command=tex.yview)
        tex["yscrollcommand"] = scro
        tex.insert(tk.END, self.get_help_message())
        tex['state'] = tk.DISABLED
        but = tk.Button(self, text="Ok", command=self.clic_ok)

        but.pack(side=tk.BOTTOM)
        tex.pack(side=tk.LEFT)
        scro.pack(side=tk.RIGHT, expand=True, fill=tk.BOTH)



    @staticmethod
    def display_help(parent):
        """
        Lance la fenetre modal de l'aide.

        params:
          - parent: le parent du GUI
        """
        top = ModalHelp(parent)
        #while top.state() != "destroyed":
        #    time.sleep(1)



    def clic_ok(self):
        """
        Gestion du click sur OK.
        """
        self.destroy()



    def get_help_message(self):
        """
        Message d'aide.
        """
        msg="""AIDE

La page est la partie blanche.

Le cadre violet représente l'espace disponible pour écriture.

Le cadre bleu est la boite de titre.
Bleu foncée: elle est trop petite pour afficher
    correctement le texte.
Boite barrée: il n'y a pas de texte à afficher.

Les cadres roses sont les cadres réservés aux photos.
Les photos sont agrandies ou diminuées pour rentrer
dans la boite, en conservant le ratio.
Boite foncée: pas de photo.
Boite barrée: la photo est considérée comme chaise vide.

Les cadres verts sont les cadres réservés aux noms des élèves.
Ils sont écrits sur 2 ou 3 lignes.
Boite foncée: le texte est trop grand pour rentrer dans la boite.
Boite barrée: l'élève n'a pas de de nom.

"""
        return msg




#
#
# END
#
#
